<?php

declare(strict_types=1);

use Tester\TestCase;

require __DIR__ . '/bootstrap.php';

class AuthorizatorTest extends TestCase
{
    public function testConstructor()
    {
        $userManagerService = new MockUserManagerService();
        $auth = new \Mepatek\UserManager\Authorizator($userManagerService);
        \Tester\Assert::type(\Mepatek\UserManager\Authorizator::class, $auth);
        $roles = [];
        for ($i=1;$i<=8;$i++) {
            $roles[] = "role$i";
        }
        \Tester\Assert::equal($roles,$auth->getRoles());
    }
}

class MockUserManagerService extends \Mepatek\UserManager\Model\UserManagerService
{
    public function __construct()
    {
    }

    public function getRoleFacade(): \Mepatek\UserManager\Model\Facade\RoleFacade
    {
        return new MockRoleFacade();
    }

    public function getResourceFacade(): \Mepatek\UserManager\Model\Facade\ResourceFacade
    {
        return new MockResourceFacade();
    }

    public function getAclFacade(): \Mepatek\UserManager\Model\Facade\AclFacade
    {
        return new MockAclFacade();
    }
}

class MockRoleFacade extends \Mepatek\UserManager\Model\Facade\RoleFacade
{
    public function __construct()
    {
    }

    public function getCachedRoles(): array
    {
        /** @var \Mepatek\UserManager\Model\Role[] $roles */
        $roles = [];
        for ($i=1;$i<=8;$i++) {
            $roles[$i] = (new \Mepatek\UserManager\Model\Role())
                ->setId($i)
                ->setName("Role $i")
                ->setRole("role$i")
                ->setDescription("description $i");
        }
        // relation between roles
        $roles[3]->setParentRoles([$roles[4]]);
        $roles[6]->setParentRoles([$roles[5]]);
        $roles[7]->setParentRoles([$roles[6]]);
        $roles[8]->setParentRoles([$roles[4]]);
        return $roles;
    }
}

class MockResourceFacade extends \Mepatek\UserManager\Model\Facade\ResourceFacade
{
    public function __construct()
    {
    }

    public function getResources(): array
    {
        return [
            (new \Mepatek\UserManager\Model\ResourceObject())
                ->setResource("resource1")
                ->setPrivileges(["view" => "show all", "edit" => "edit all"]),
        ];
    }
}

class MockAclFacade extends \Mepatek\UserManager\Model\Facade\AclFacade
{
    public function __construct()
    {
    }

    public function getCachedAcls(): array
    {
        return [];
    }
}

(new AuthorizatorTest)->run();