<?php
declare(strict_types=1);

namespace Mepatek\UserManager;

use DateInterval;
use DateTime;
use Exception;
use Mepatek\UserManager\AuthDrivers\IAuthDriver;
use Mepatek\UserManager\Exception\UserManagerException;
use Mepatek\UserManager\Model\User;
use Mepatek\UserManager\Model\UserActivity;
use Mepatek\UserManager\Model\UserManagerService;
use Nette\Security\AuthenticationException;
use Nette\Security\IAuthenticator;
use Nette\Security\Identity;
use Nette\Security\IIdentity;
use Nette\Security\Passwords;

/**
 * Users authenticator.
 */
class Authenticator implements IAuthenticator
{
    /**
     * Minimum password length
     * @var integer
     */
    public $passwordMinLength = 8;
    /**
     * Minimum password level
     * @var integer
     */
    public $passwordMinLevel = 2;
    /** @var IAuthDriver[] */
    protected $authDrivers = [];
    /** @var UserManagerService */
    private $userManagerService;
    /** @var Passwords */
    private $passwords;
    /** @var string */
    private $intervalExpirePwToken = 'PT30M';

    /**
     * Authenticator constructor.
     *
     * @param UserManagerService $userManagerService
     * @param Passwords $passwords
     */
    public function __construct(
        UserManagerService $userManagerService,
        Passwords $passwords
    ) {
        $this->userManagerService = $userManagerService;
        $this->passwords = $passwords;
    }

    /**
     * Performs an authentication.
     *
     * @param array $credentials
     * @return IIdentity
     * @throws AuthenticationException
     * @throws UserManagerException
     */
    public function authenticate(array $credentials): IIdentity
    {
        /** @var User|null $user */
        $user = null;

        if (count($credentials) == 2) {
            list($username, $password) = $credentials;
            foreach ($this->authDrivers as $authDriver) {
                if ($authDriver->authenticateLoop()) {
                    $user = $authDriver->authenticate($username, $password);
                }
                if ($user) {
                    break;
                }
            }
        } else {
            $matches = null;
            $authMethod = null;
            if (preg_match("~:(.{1,30}):~", $credentials[0], $matches)) {
                $authDriverName = $matches[1];
            } else {
                throw new UserManagerException("Chybně zadaná ověřovací metoda");
            }
            /** @var IAuthDriver $authDriver */
            $authDriver = $this->getAuthDriverByName($authDriverName);
            $user = $authDriver->authenticate("", "");
        }

        if (!$user instanceof User) {
            $this->userManagerService->warning(
                "Chybný pokus o přihlášení do systému (IP: {ip}, username: {username}).",
                [
                    "ip" => UserActivity::getRemoteIp(),
                    "username" => $credentials[0],
                ]
            );
            throw new AuthenticationException(
                'Uživatel nemá do systému povolen přístup. Kontaktujte správce.',
                self::IDENTITY_NOT_FOUND
            );
        }

        if ($user->isDisabled()) {
            $this->userManagerService->warning(
                "Uživatel {username} je zakázaný a pokusil se o přístup do systému.",
                [
                    "username" => $user->getUserName(),
                ]
            );
            throw new AuthenticationException(
                'Uživatel nemá do systému povolen přístup. Kontaktujte správce.',
                self::NOT_APPROVED
            );
        }
        if ($user->isDeleted()) {
            $this->userManagerService->warning(
                "Uživatel {username} je smazaný a pokusil se o přístup do systému.",
                [
                    "username" => $user->getUserName(),
                ]
            );
            throw new AuthenticationException(
                'Uživatel nemá do systému povolen přístup. Kontaktujte správce.',
                self::NOT_APPROVED
            );
        }

        // update lastLogged
        $user->setLastLogged(new DateTime());
        $user->addActivity(UserActivity::TYPE_LOGIN);
        try {
            $this->userManagerService->getUserFacade()->saveUser($user);
        } catch (Exception $exception) {
            throw new UserManagerException(
                "Nepodařil se zápis do databáze (" . $exception->getMessage() . ")"
            );
        }

        $this->userManagerService->info(
            "Uživatel {fullname} ({userid}) přihlášen.",
            [
                "fullname" => $user->getFullName(),
                "userid" => $user->getId(),
            ]
        );
        $identity = new Identity($user->getId(), $user->getIdentityRoles(), $user->getIdentityData());
        return $identity;
    }

    /**
     * Log logout activity
     *
     * @param integer $id
     * @throws UserManagerException
     */
    public function logout(int $id): void
    {
        /** @var User $user */
        $user = $this->userManagerService->getUserFacade()->find($id);
        if ($user) {
            $user->addActivity(UserActivity::TYPE_LOGOUT);
            try {
                $this->userManagerService->getUserFacade()->saveUser($user);
            } catch (Exception $exception) {
                throw new UserManagerException(
                    "Nepodařil se zápis do databáze (" . $exception->getMessage() . ")"
                );
            }
            $this->userManagerService->info(
                "Uživatel {fullname} ({userid}) odhlášen.",
                [
                    "fullname" => $user->getFullName(),
                    "userid" => $user->getId(),
                ]
            );
        }
    }

    /**
     * Invite user
     * If user not exists then disable and create else save
     *
     * @param User $user
     * @return bool
     */
    public function inviteUser(User $user): bool
    {
        try {
            $user->setInvited();
            $this->userManagerService->getUserFacade()->saveUser($user);
        } catch (Exception $e) {
            return false;
        }
        $this->userManagerService->info(
            "Uživatel {email} ({userid}) byl pozván.",
            [
                "email" => $user->getEmail(),
                "userid" => $user->getId(),
            ]
        );
        return true;
    }

    /**
     * Generate token for change password.
     *
     * @param string $email
     * @return string|null
     */
    public function resetPasswordToken(string $email): ?string
    {
        $user = $this->userManagerService->getUserFacade()->findUserByEmail($email);
        // userExist?
        if ($user) {
            try {
                $token = $this->userManagerService->getUserFacade()
                    ->resetUserPwToken($user, new DateInterval($this->intervalExpirePwToken));
                $user->addActivity(UserActivity::TYPE_PWTOKEN_GEN);
                $this->userManagerService->getUserFacade()->saveUser($user);
            } catch (Exception $exception) {
                $token = null;
            }
            return $token ? $token : null;
        } else {
            return null;
        }
    }

    /**
     * Change password for $token
     *
     * @param string $token
     * @param string $newPassword
     * @return User|null
     * @throws UserManagerException
     */
    public function changePasswordToken(string $token, string $newPassword): ?User
    {
        /** @var User $user */
        $user = $this->userManagerService->getUserFacade()->findUserByPwToken($token);
        if ($user) {
            return $this->changePassword($user->getId(), $newPassword);
        }
        return $user;
    }

    /**
     * Change password user find by id
     *
     * @param int $id
     * @param string $newPassword
     * @return User|null
     * @throws UserManagerException
     */
    public function changePassword(int $id, string $newPassword): ?User
    {
        $user = $this->userManagerService->getUserFacade()->find($id);
        if ($user) {
            $changed = false;
            foreach ($this->authDrivers as $authDriver) {
                if ($authDriver->hasChangePassword()) {
                    $changed = $authDriver->changePassword($user, $newPassword);
                }
            }
            if ($changed) {
                $user->addActivity(UserActivity::TYPE_CHANGE_PASSWORD);
                $user->setPwToken(null);
                $user->setPwTokenExpire(null);
                try {
                    $this->userManagerService->getUserFacade()->saveUser($user);
                } catch (Exception $exception) {
                    throw new UserManagerException(
                        "Nepodařilo se změnit heslo uživatele (" . $exception->getMessage() . ")"
                    );
                }
            } else {
                $user = null;
            }
        }
        return $user;
    }

    /**
     * Check password length and check password complexity
     *
     * @param string $password
     *
     * @return int 0 -password is OK, 2 -password is short, 4 -password is not safe, 6 -password is short and not safe
     */
    public function isPasswordSafe(string $password): int
    {
        $passwordLevel = 0;

        if (preg_match('`[A-Z]`', $password)) { // at least one big sign
            $passwordLevel++;
        }
        if (preg_match('`[a-z]`', $password)) { // at least one small sign
            $passwordLevel++;
        }
        if (preg_match('`[0-9]`', $password)) { // at least one digit
            $passwordLevel++;
        }
        if (preg_match('`[-!"#$%&\'()* +,./:;<=>?@\[\]\\\\^_\`{|}~]`', $password)) { // at least one special character
            $passwordLevel++;
        }

        $retValue = 0;

        if ($this->passwordMinLength > strlen($password)) {
            $retValue += 2;
        }
        if ($this->passwordMinLevel > $passwordLevel) {
            $retValue += 4;
        }

        return $retValue;
    }

    /**
     * Generate random password with length
     *
     * @param integer $length length of password
     *
     * @return string
     */
    public function generateRandomPassword(int $length = null): string
    {
        $length = max((int)$length, $this->passwordMinLevel);
        $sets = [];
        if ($this->passwordMinLevel >= 1) {
            $sets[] = 'abcdefghijklmnopqrstuvwxyz';
        }
        if ($this->passwordMinLevel >= 2) {
            $sets[] = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
        }
        if ($this->passwordMinLevel >= 3) {
            $sets[] = '0123456789';
        }
        if ($this->passwordMinLevel >= 4) {
            $sets[] = '!@#$%&*?';
        }

        $all = '';
        $password = '';
        foreach ($sets as $set) {
            $password .= $set[array_rand(str_split($set))];
            $all .= $set;
        }
        $all = str_split($all);
        for ($i = 0; $i < $length - count($sets); $i++) {
            $password .= $all[array_rand($all)];
        }
        $password = str_shuffle($password);
        return $password;
    }

    /**
     * Add authDriver
     *
     * @param IAuthDriver $authDriver
     */
    public function addAuthDriver(IAuthDriver $authDriver): void
    {
        $this->authDrivers[$authDriver->getName()] = $authDriver;

        $authDriver->setUp(
            $this->userManagerService,
            $this->passwords
        );
    }

    /**
     * @param string $name
     * @return IAuthDriver|null
     */
    public function getAuthDriverByName(string $name): ?IAuthDriver
    {
        return key_exists($name, $this->authDrivers) ?
            $this->authDrivers[$name] : null;
    }

    /**
     * @return string
     */
    public function getIntervalExpirePwToken(): string
    {
        return $this->intervalExpirePwToken;
    }

    /**
     * @param string $intervalExpirePwToken
     * @return Authenticator
     */
    public function setIntervalExpirePwToken(string $intervalExpirePwToken): Authenticator
    {
        $this->intervalExpirePwToken = $intervalExpirePwToken;
        return $this;
    }
}
