<?php
declare(strict_types=1);

namespace Mepatek\UserManager\AuthDrivers;

use Contributte\Events\Extra\Event\Application\StartupEvent;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Exception;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Psr7\Stream;
use League\OAuth2\Client\Provider\GenericProvider;
use League\OAuth2\Client\Token\AccessTokenInterface;
use Mepatek\UserManager\Authenticator;
use Mepatek\UserManager\Model\AuthDriver;
use Mepatek\UserManager\Model\Role;
use Mepatek\UserManager\Model\User;
use Mepatek\UserManager\Model\UserManagerService;
use Microsoft\Graph\Graph;
use Microsoft\Graph\Model\User as AzureUser;
use Nette\Application\LinkGenerator;
use Nette\Application\UI\InvalidLinkException;
use Nette\Http\Session;
use Nette\Http\SessionSection;
use Nette\Security\AuthenticationException;
use Nette\Security\Passwords;
use Nette\Utils\Strings;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Class AzureOAuth2AuthDriver
 *
 * @package Mepatek\UserManager\AuthDrivers
 */
class AzureOAuth2AuthDriver implements IAuthDriver, EventSubscriberInterface
{
    /** @var array */
    private $options = [];
    /** @var string */
    private $loginAction;
    /** @var string */
    private $redirectAfterAuthenticate;
    /** @var LinkGenerator */
    private $linkGenerator;
    /** @var SessionSection */
    private $sessionSection;
    /** @var UserManagerService */
    private $userManagerService;
    /**
     * True:
     *   if user not exist, find rolesForNewUser, if found any role create user
     * False:
     *   if user not exist, not create
     * @var bool
     */
    private $autoAddNewUsers;
    /**
     * @var array|null
     * Structure:
     * patternEmail => role
     *  - patternEmail is regexp for email
     *  - role - string role.role
     * example:
     *   josef.dohnal@.* => admin
     *   .*@mepatek.cz => users
     *   "*"=> guest (*= if not found email in another role)
     *   "*"=> some_role
     *
     * rolesForNewUsers==null => ["*"=>guest]
     */
    private $rolesForNewUsers = null;

    /**
     * AzureOAuth2AuthDriver constructor.
     *
     * @param array $options
     * @param string $loginAction
     * @param string $redirectAfterAuthenticate
     * @param bool $autoAddNewUsers
     * @param string|null $appName
     * @param LinkGenerator $linkGenerator
     * @param Session|null $session
     */
    public function __construct(
        array $options,
        string $loginAction,
        string $redirectAfterAuthenticate,
        bool $autoAddNewUsers = false,
        string $appName = null,
        LinkGenerator $linkGenerator = null,
        Session $session = null
    ) {
        $this->options = $options;
        $this->loginAction = $loginAction;
        $this->redirectAfterAuthenticate = $redirectAfterAuthenticate;
        $this->autoAddNewUsers = $autoAddNewUsers;
        $this->linkGenerator = $linkGenerator;
        $sessionName = ($appName ?: "") . "azureOAuth2";
        $this->sessionSection = $session->getSection($sessionName);
    }

    /**
     * for Events subscriber
     *
     * @return array
     */
    public static function getSubscribedEvents(): array
    {
        return [StartupEvent::class => "onStartup"];
    }

    /**
     * for Events subscriber
     * Refresh token if needed
     * @param StartupEvent $event
     * @throws InvalidLinkException
     */
    public function onStartup(StartupEvent $event)
    {
        $this->refreshToken();
    }

    /**
     * Refresh token if needed
     * @throws InvalidLinkException
     */
    public function refreshToken()
    {
        $accessToken = $this->getAccessToken();

        if ($accessToken and $accessToken->hasExpired()) {
            try {
                $accessToken = $this->getProvider()
                    ->getAccessToken(
                        'refresh_token',
                        [
                            'refresh_token' => $accessToken->getRefreshToken(),
                        ]
                    );
                $this->setAccessToken($accessToken);
            } catch (Exception $e) {
                $redirectUrl = $this->linkGenerator->link($this->loginAction, ["azure" => true]);
                header('Location: ' . $redirectUrl);
            }
        }
    }

    /**
     * @param string|null $accessToken
     * @return Graph
     * @throws InvalidLinkException
     */
    public function getGraph(string $accessToken = null): Graph
    {
        static $graph = null;
        if ($graph) {
            return $graph;
        }
        $graph = new Graph();
        $graph->setAccessToken($accessToken ?: $this->getToken());
        return $graph;
    }

    /**
     * Get current azure Token
     * @return string|null
     * @throws InvalidLinkException
     */
    public function getToken(): ?string
    {
        $this->refreshToken();
        return $this->sessionSection->accessToken
            ? $this->sessionSection->accessToken->getToken()
            : null;
    }

    /**
     * Get azure app Token
     * @return string|null
     * @throws GuzzleException
     */
    public function getAppToken(): ?string
    {
        $token = null;
        $client = new Client();
        $response = $client->request(
            "POST",
            $this->options["urlAccessToken"],
            [
                "form_params" => [
                    "client_id" => $this->options["clientId"],
                    "scope" => "https://graph.microsoft.com/.default",
                    "client_secret" => $this->options["clientSecret"],
                    "grant_type" => "client_credentials",
                ],
            ]
        );
        if ($response->getStatusCode() == 200) {
            $tokenArray = json_decode($response->getBody()->getContents(), true);
            $token = isset($tokenArray["access_token"]) ? $tokenArray["access_token"] : null;
        }

        return $token;
    }

    /**
     * @param string $username
     * @param string $password
     * @return User
     * @throws InvalidLinkException
     * @throws ORMException
     * @throws OptimisticLockException
     * @throws AuthenticationException
     */
    public function authenticate(
        string $username,
        string $password
    ): ?User {
        $user = null;

        if ($this->sessionSection->logged) {
            $graph = $this->getGraph();
            try {
                /** @var AzureUser $userAzure */
                $ednpoint = "/me";
                $ednpoint .= "?\$select=id,mail,mobilePhone,displayName,userPrincipalName";
                $userAzure = $graph->createRequest("GET", $ednpoint)
                    ->addHeaders(["Content-Type" => "application/json"])
                    ->setReturnType(AzureUser::class)
                    ->execute();
            } catch (Exception $e) {
                $this->userManagerService->error(
                    "Načtení informací o uživateli z Azure AD selhalo. " .
                    $e->getMessage()
                );
                throw new AuthenticationException('Nepodařilo se načíst informace z Azure AD.', Authenticator::FAILURE);
            }

            $email = $userAzure->getMail();
            $authId = $userAzure->getId();

            $user = $this->userManagerService
                ->getUserFacade()
                ->findUserByAuthDriverAndAuthId(
                    $this->getName(),
                    $authId,
                    true,
                    true,
                    true
                );

            if ($user === null) {
                $user = $this->userManagerService
                    ->getUserFacade()
                    ->findUserByEmail($email);
            }

            if ($user and $user->isDeleted()) {
                return $user;
            }

            if ($user === null and
                $this->autoAddNewUsers and
                count($this->getRoleForNewUser($email)) > 0
            ) {
                $user = $this->createUserFromAzure($userAzure);
            } else {
                if ($user) {
                    $this->updateUserFromAzure($userAzure, $user);
                }
            }

            if ($user !== null) {
                //                if ($this->autoUpdateRole) {
                //                    $this->updateRole($user, $adUser);
                //                }
                if (!($authDriver = $user->getAuthDriverByName($this->getName()))) {
                    $authDriver = new AuthDriver();
                    $authDriver->setAuthDriver($this->getName());
                    $user->addAuthDriver($authDriver);
                }
                $authDriver->setAuthId($authId);
                $this->userManagerService
                    ->getUserFacade()
                    ->saveUser($user);
            }
        }
        return $user;
    }

    /**
     * Set Up event
     *
     * @param UserManagerService $userManagerService
     * @param Passwords $passwords
     * @throws InvalidLinkException
     */
    public function setUp(
        UserManagerService $userManagerService,
        Passwords $passwords
    ): void {
        $this->userManagerService = $userManagerService;

        if (isset($_GET["azure"]) or isset($_GET['code'])) {
            $this->oauth();
        }
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return "AzureOAuth2";
    }

    /**
     * @return bool
     */
    public function authenticateLoop(): bool
    {
        return false;
    }

    /**
     * @param User $user
     * @param string $newPassword
     * @return bool
     */
    public function changePassword(User $user, string $newPassword): bool
    {
        return false;
    }

    /**
     * @return bool
     */
    public function hasChangePassword(): bool
    {
        return false;
    }

    /**
     * @return bool
     */
    public function isAutoAddNewUsers(): bool
    {
        return $this->autoAddNewUsers;
    }

    /**
     * @param bool $autoAddNewUsers
     * @return AzureOAuth2AuthDriver
     */
    public function setAutoAddNewUsers(bool $autoAddNewUsers): AzureOAuth2AuthDriver
    {
        $this->autoAddNewUsers = $autoAddNewUsers;
        return $this;
    }

    /**
     * @return array
     */
    public function getRolesForNewUsers(): array
    {
        return is_array($this->rolesForNewUsers) ? $this->rolesForNewUsers : ["*" => "guest"];
    }

    /**
     * @param array|null $rolesForNewUsers
     * @return AzureOAuth2AuthDriver
     */
    public function setRolesForNewUsers(?array $rolesForNewUsers): AzureOAuth2AuthDriver
    {
        $this->rolesForNewUsers = $rolesForNewUsers;
        return $this;
    }

    /**
     * @return AccessTokenInterface|null
     */
    public function getAccessToken(): ?AccessTokenInterface
    {
        /** @var AccessTokenInterface $accessToken */
        $accessToken = null;
        $accessToken = $this->sessionSection->accessToken;
        return $accessToken;
    }

    /**
     * Create User Entity from adUser
     *
     * @param AzureUser $userAzure
     *
     * @return User|null
     */
    private function createUserFromAzure(AzureUser $userAzure): ?User
    {
        $user = new User();
        $user->addActivity(
            "createFromAuthDriver",
            "Auto create from " . $this->getName()
        );

        try {
            $roles = $this->getRoleForNewUser($userAzure->getMail());
            foreach ($roles as $role) {
                $user->addRole($role);
            }
        } catch (Exception $e) {
            $this->userManagerService->error(
                "azureAdcreateUserFromAzure: Nepodařilo se načíst role. " .
                $e->getMessage()
            );
        }
        $this->updateUserFromAzure($userAzure, $user);

        // save user
        try {
            $this->userManagerService
                ->getUserFacade()
                ->saveUser($user);
        } catch (Exception $e) {
            $user = null;
        }

        return $user;
    }

    /**
     * @param string $email
     * @return array
     * @throws ORMException
     * @throws OptimisticLockException
     */
    private function getRoleForNewUser(string $email): array
    {
        static $rolesByEmail = [];
        if (!key_exists($email, $rolesByEmail)) {
            $rolesByEmail[$email] = [];

            static $rolesForOthers = null;
            if ($rolesForOthers === null) {
                $rolesForOthers = [];
                foreach ($this->getRolesForNewUsers() as $emailPattern => $roleName) {
                    if (trim($emailPattern) === "*") {
                        $rolesForOthers[] = $roleName;
                    }
                }
            }
            $roles = [];
            foreach ($this->getRolesForNewUsers() as $emailPattern => $roleName) {
                if (trim($emailPattern) !== "*" and Strings::match($email, "~$emailPattern~")) {
                    $roles[] = $roleName;
                }
            }

            if (count($roles) == 0) {
                $roles = $rolesForOthers;
            }

            foreach ($roles as $roleName) {
                $role = $this->userManagerService->getRoleFacade()->findByRole($roleName);
                if (!$role) {
                    $role = new Role();
                    $role->setRole($roleName);
                    $this->userManagerService->getRoleFacade()->saveRole($role);
                }
                $rolesByEmail[$email][] = $role;
            }
        }
        return $rolesByEmail[$email];
    }

    /**
     * Update User Entity from Azure data
     *
     * @param AzureUser $userAzure
     * @param User $user
     *
     * @return bool
     */
    private function updateUserFromAzure(AzureUser $userAzure, User $user): bool
    {
        $userChanges = false;
        if ($userAzure->getDisplayName() !== $user->getFullName()) {
            $user->setFullName($userAzure->getDisplayName());
            $userChanges = true;
        }
        if (!$user->getUserName()) {
            $user->setUserName($userAzure->getUserPrincipalName());
            $userChanges = true;
        }
        if ($userAzure->getMail() !== $user->getEmail()) {
            $user->setEmail($userAzure->getMail());
            $userChanges = true;
        }
        if ($userAzure->getMobilePhone() !== $user->getPhone()) {
            $user->setPhone($userAzure->getMobilePhone());
            $userChanges = true;
        }

        if (!$user->hasThumbnail()) {
            try {
                $photo = $this->getGraph()
                    ->createRequest("GET", '/me/photos/240x240/$value')
                    ->addHeaders(["Content-Type" => "image/jpeg"])
                    ->setReturnType(Stream::class)
                    ->execute();
                $thumbnail = $photo ? $photo->getContents() : null;
                if ($thumbnail) {
                    $user->setThumbnail($thumbnail);
                    $userChanges = true;
                }
            } catch (Exception $e) {
                $this->userManagerService->warning(
                    "updateUserFromAzure: Nepodařilo se načíst obrázek z profilu. " .
                    $e->getMessage()
                );
            }
        }

        if ($userChanges) {
            // save user
            try {
                $user->addActivity(
                    "updateFromAuthDriver",
                    "Update user data from " . $this->getName()
                );
                // save user
                $this->userManagerService
                    ->getUserFacade()
                    ->saveUser($user);
            } catch (Exception $e) {
                $user = null;
                $this->userManagerService->warning(
                    "updateUserFromAzure: Nepodařilo se uložit uživatele. " .
                    $e->getMessage()
                );
            }
        }
        return $userChanges;
    }

    /**
     * @return GenericProvider
     */
    private function getProvider(): GenericProvider
    {
        return new GenericProvider(
            $this->options
        );
    }

    /**
     * @param AccessTokenInterface $accessToken
     */
    private function setAccessToken(AccessTokenInterface $accessToken)
    {
        // The id token is a JWT token that contains information about the user
        // It's a base64 coded string that has a header, payload and signature
        $idToken = $accessToken->getValues()['id_token'];
        $decodedAccessTokenPayload = base64_decode(
            explode('.', $idToken)[1]
        );
        $jsonAccessTokenPayload = json_decode($decodedAccessTokenPayload, true);
        // The following user properties are needed in the next page
        $this->sessionSection->accessToken = $accessToken;
        $this->sessionSection->logged = true;
        $this->sessionSection->jsonAccessTokenPayload = $jsonAccessTokenPayload;
    }

    /**
     * @throws InvalidLinkException
     */
    private function oauth()
    {
        //We store user name, id, and tokens in session variables
        $provider = $this->getProvider();

        if ($_SERVER['REQUEST_METHOD'] === 'GET' && !isset($_GET['code'])) {
            $authorizationUrl = $provider->getAuthorizationUrl();
            // The OAuth library automaticaly generates a state value that we can
            // validate later. We just save it for now.
            $this->sessionSection->state = $provider->getState();
            header('Location: ' . $authorizationUrl);
            exit();
        } elseif ($_SERVER['REQUEST_METHOD'] === 'GET' && isset($_GET['code'])) {
            // Validate the OAuth state parameter
            if (empty($_GET['state']) || ($_GET['state'] !== $this->sessionSection->state)) {
                unset($this->sessionSection->state);
                $this->userManagerService->error(
                    "azureAdOauth: State není stejný jako při odeslání."
                );
                //TODO: hláška uživateli a přesměrování
            }
            // With the authorization code, we can retrieve access tokens and other data.
            try {
                // Get an access token using the authorization code grant
                $accessToken = $provider->getAccessToken('authorization_code', [
                    'code' => $_GET['code'],
                ]);
                $this->setAccessToken($accessToken);
                $redirectUrl = $this->linkGenerator->link($this->redirectAfterAuthenticate);
                header('Location: ' . $redirectUrl);
                exit();
            } catch (\League\OAuth2\Client\Provider\Exception\IdentityProviderException $e) {
                $this->userManagerService->error(
                    "azureAdOauth: Nepodařilo se získat token uživatel. " .
                    $e->getMessage()
                );
                //TODO: hláška uživateli a přesměrování
            }
        }
    }
}
