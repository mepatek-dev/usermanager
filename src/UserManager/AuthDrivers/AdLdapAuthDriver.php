<?php
declare(strict_types=1);

namespace Mepatek\UserManager\AuthDrivers;

use Adldap\Adldap;
use Adldap\Models;
use Mepatek\UserManager\Model\AuthDriver;
use Mepatek\UserManager\Model\Role;
use Mepatek\UserManager\Model\User;
use Mepatek\UserManager\Model\UserManagerService;
use Nette\Security\Passwords;

/**
 * Class AdLdapAuthDriver
 *
 * @package Mepatek\UserManager\AuthDrivers
 */
final class AdLdapAuthDriver implements IAuthDriver
{
    /** @var Adldap */
    protected $ad = null;
    /** @var array */
    protected $adConfig;
    /**
     * @var false|true|array
     * true = all users
     * array = user in group [group1, group2, group3, ...]
     */
    protected $autoAddNewUsersInGroups;
    /** @var boolean true = auto update roles in authenticate method */
    protected $autoUpdateRole = false;
    /** @var boolean true = auto create role if not exist */
    protected $autoCreateRole = false;
    /** @var array group=>role mapping */
    protected $group2Role;
    /** @var UserManagerService */
    private $userManagerService;

    /**
     * AdLdapAuthDriver constructor.
     *
     * @param array $adConfig
     * @param bool $autoUpdateRole
     * @param bool $autoCreateRole
     * @param null|true|array $autoAddNewUsersInGroups
     * @param array $group2Role
     */
    public function __construct(
        array $adConfig,
        $autoUpdateRole = false,
        $autoCreateRole = false,
        $autoAddNewUsersInGroups = null,
        $group2Role = []
    ) {
        $this->adConfig = $adConfig;
        $this->autoUpdateRole = $autoUpdateRole;
        $this->autoCreateRole = $autoCreateRole;
        $this->autoAddNewUsersInGroups = $autoAddNewUsersInGroups;
        $this->group2Role = $group2Role;
    }

    /**
     * Set Up event
     *
     * @param UserManagerService $userManagerService
     * @param Passwords $passwords
     */
    public function setUp(
        UserManagerService $userManagerService,
        Passwords $passwords
    ): void {
        $this->userManagerService = $userManagerService;
    }

    /**
     * @param string $username
     * @param string $password
     *
     * @return bool
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     * @throws \Doctrine\ORM\TransactionRequiredException
     */
    public function authenticate(string $username, string $password): ?User
    {
        if ($this->ad === null) {
            $this->ad = new Adldap($this->adConfig);
        }
        $user = null;
        if ($this->ad->auth()->attempt($username, $password, true)) {
            /** @var Models\User $adUser */
            $adUser = $this->ad->search()->find($username);
            $sid = $adUser->getConvertedSid();

            $user = $this->userManagerService
                ->getUserFacade()
                ->findUserByAuthDriverAndAuthId(
                    $this->getName(),
                    $sid
                );
            if ($user === null and $this->hasAutoAddUser($adUser)) {
                $user = $this->userManagerService
                    ->getUserFacade()
                    ->findUserByUserName($username);
                if (!$user) {
                    $user = $this->createUserFromAd($adUser);
                }
            }

            if ($user !== null) {
                if ($this->autoUpdateRole) {
                    $this->updateRole($user, $adUser);
                }
                if (!($authDriver = $user->getAuthDriverByName($this->getName()))) {
                    $authDriver = new AuthDriver();
                    $authDriver->setAuthDriver($this->getName());
                    $user->addAuthDriver($authDriver);
                }
                $authDriver->setAuthId($sid);
                $this->userManagerService
                    ->getUserFacade()
                    ->saveUser($user);
            }
        }
        return $user;
    }

    /**
     * Get auth driver name (max 30char)
     *
     * @return string
     */
    public function getName(): string
    {
        return "AdLDAP";
    }

    /**
     * Start automatically in authenticate loop?
     * @return bool
     */
    public function authenticateLoop(): bool
    {
        return true;
    }

    /**
     * @param string $username
     * @param string $authId
     * @param string $newPassword
     *
     * @return boolean
     */
    public function changePassword(User $user, string $newPassword): bool
    {
        return false;
    }

    /**
     * @return boolean
     */
    public function hasChangePassword(): bool
    {
        return false;
    }

    /**
     * If autoAddNewUsersInGroups==true or user member of grou in autoAddNewUsersInGroups return true
     *
     * @param Models\User $adUser
     *
     * @return boolean
     */
    protected function hasAutoAddUser(Models\User $adUser): bool
    {
        if ($this->autoAddNewUsersInGroups === true) {
            return true;
        }
        if (!is_array($this->autoAddNewUsersInGroups)) {
            return false;
        }

        foreach ($adUser->getGroupNames() as $group) {
            //          $group = \Adldap\Classes\Utilities::unescape($group);
            if (in_array($group, $this->autoAddNewUsersInGroups, true)) {
                return true;
            }
        }
        return false;
    }

    /**
     * Create User Entity from adUser
     *
     * @param Models\User $adUser
     *
     * @return null|user
     */
    protected function createUserFromAd(Models\User $adUser): ?User
    {
        $user = new User();

        $user->setFullName($adUser->getDisplayName());
        $user->setUserName($adUser->getAccountName());
        $user->setEmail($adUser->getEmail());
        $user->setPhone($adUser->getTelephoneNumber());
        $user->setTitle($adUser->getTitle());
        $user->setThumbnail($adUser->getThumbnail());

        // save user
        try {
            $user->addActivity(
                "createFromAuthDriver",
                "Auto create from " . $this->getName()
            );
            $this->userManagerService
                ->getUserFacade()->saveUser($user);
        } catch (\Exception $e) {
            $user = null;
        }

        return $user;
    }

    /**
     * Update roles
     *
     * @param User $user
     * @param Models\User $adUser
     *
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     * @throws \Doctrine\ORM\TransactionRequiredException
     */
    protected function updateRole(User $user, Models\User $adUser): void
    {
        $memberOf = $adUser->getGroupNames();
        foreach ($this->group2Role as $group => $role) {
            if (in_array($group, $memberOf, true)) {
                if ($this->roleExists($role)) {
                    if (!$user->isInRole($role)) {
                        /** @var Role $roleObj */
                        $roleObj = $this->em->find(Role::class, $role);
                        $user->addRole($roleObj);
                    }
                }
            }
        }
    }

    /**
     * True if role exists
     * If not exists and autoCreateRole=true, create it
     *
     * @param string $roleId
     *
     * @return boolean
     *
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     * @throws \Doctrine\ORM\TransactionRequiredException
     */
    protected function roleExists(string $roleId): bool
    {
        static $roles = null;
        if ($roles == null) {
            $roles = $this->userManagerService
                ->getRoleFacade()
                ->getCachedRoles();
        }
        if (key_exists($roleId, $roles) and $this->autoCreateRole) {
            $role = new Role();
            $role->setRole($roleId);
            $this->userManagerService
                ->getRoleFacade()
                ->save($role);
        }
        return $role ? true : false;
    }
}
