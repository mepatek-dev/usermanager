<?php
declare(strict_types=1);

namespace Mepatek\UserManager\AuthDrivers;

use Mepatek\UserManager\Model\UserManagerService;
use Mepatek\UserManager\Model\User;
use Nette\Security\Passwords;

/**
 * Class DbAuthDriver
 * @package Mepatek\UserManager\AuthDrivers
 */
final class DbAuthDriver implements IAuthDriver
{
    /** @var UserManagerService */
    private $userManagerService;
    /** @var Passwords */
    private $passwords;

    /**
     * Set Up event
     * @param UserManagerService $userManagerService
     * @param Passwords $passwords
     */
    public function setUp(
        UserManagerService $userManagerService,
        Passwords $passwords
    ):void {
        $this->userManagerService = $userManagerService;
        $this->passwords = $passwords;
    }

    /**
     * @param string $username
     * @param string $password
     * @return User|null
     */
    public function authenticate(string $username, string $password): ?User
    {
        $user = $this->userManagerService->getUserFacade()->findUserByUserName($username);
        if ($user) {
            if ($this->passwords->verify($password, $user->getPwHash())) {
                if ($this->passwords->needsRehash($user->getPwHash())) {
                    $this->changePassword($user, $password);
                }
            } else {
                $user = null;
            }
        }
        return $user;
    }

    /**
     * Get auth driver name (max 30char)
     * @return string
     */
    public function getName(): string
    {
        return "db";
    }

    /**
     * Start automatically in authenticate loop?
     * @return bool
     */
    public function authenticateLoop(): bool
    {
        return true;
    }

    /**
     * @param User $user
     * @param string $newPassword
     *
     * @return bool
     */
    public function changePassword(User $user, string $newPassword): bool
    {
        $user->setPwHash($this->passwords->hash($newPassword));
        return true;
    }

    /**
     * @return bool
     */
    public function hasChangePassword(): bool
    {
        return true;
    }
}