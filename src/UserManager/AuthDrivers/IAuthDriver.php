<?php
declare(strict_types=1);

namespace Mepatek\UserManager\AuthDrivers;

use Mepatek\UserManager\Model\User;
use Mepatek\UserManager\Model\UserManagerService;
use Nette\Security\Passwords;

/**
 * Interface IAuthDriver
 *
 * @package Mepatek\UserManager\AuthDrivers
 */
interface IAuthDriver
{
    /**
     * Set Up event
     *
     * @param UserManagerService $userManagerService
     * @param Passwords $passwords
     */
    public function setUp(UserManagerService $userManagerService, Passwords $passwords): void;

    /**
     * @param string $username
     * @param string $password
     * @return User|null
     */
    public function authenticate(string $username, string $password): ?User;

    /**
     * Get auth driver name (max 30char)
     * @return string
     */
    public function getName(): string;

    /**
     * Start automatically in authenticate loop?
     * @return bool
     */
    public function authenticateLoop(): bool;

    /**
     * @param User $user
     * @param string $newPassword
     *
     * @return bool
     */
    public function changePassword(User $user, string $newPassword): bool;

    /**
     * @return bool
     */
    public function hasChangePassword(): bool;
}
