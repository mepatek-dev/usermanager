<?php
declare(strict_types=1);

namespace Mepatek\UserManager\Entity\Helper;

/**
 * Class UserLink
 * @package Mepatek\UserManager\Entity\Helper
 */
class UserLink
{
    public $description;
    public $link=null;
    public $counter=null;
}
