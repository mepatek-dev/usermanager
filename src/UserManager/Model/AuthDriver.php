<?php
declare(strict_types=1);

namespace Mepatek\UserManager\Model;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(
 *   indexes={
 *     @ORM\Index(name="IDX_auth_id", columns={"auth_id"}),
 * })
 *
 * @package Mepatek\UserManager\Model
 */
class AuthDriver
{
    /**
     * @ORM\Id
     * @ORM\ManyToOne(targetEntity="User", inversedBy="authDrivers")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     * @var User
     */
    private $user;
    /**
     * @ORM\Id
     * @ORM\Column(type="string", length=30)
     * @var string
     */
    private $authDriver;
    /**
     * @ORM\Column(type="string", length=255)
     * @var string
     */
    private $authId;

    /**
     * @return User
     */
    public function getUser(): User
    {
        return $this->user;
    }

    /**
     * @param User $user
     * @return AuthDriver
     */
    public function setUser(User $user): AuthDriver
    {
        $this->user = $user;
        return $this;
    }

    /**
     * @return string
     */
    public function getAuthDriver(): string
    {
        return $this->authDriver;
    }

    /**
     * @param string $authDriver
     * @return AuthDriver
     */
    public function setAuthDriver(string $authDriver): AuthDriver
    {
        $this->authDriver = $authDriver;
        return $this;
    }

    /**
     * @return string
     */
    public function getAuthId(): string
    {
        return $this->authId;
    }

    /**
     * @param string $authId
     * @return AuthDriver
     */
    public function setAuthId(string $authId): AuthDriver
    {
        $this->authId = $authId;
        return $this;
    }
}
