<?php
declare(strict_types=1);

namespace Mepatek\UserManager\Model;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(
 *   name="`acl`",
 *   indexes={
 *     @ORM\Index(name="IDX_resource", columns={"resource"}),
 *   },
*    uniqueConstraints={
 *     @ORM\UniqueConstraint(name="UQIDX_role_resource", columns={"role_id", "resource"})
 *   },
 * )
 *
 * @package Mepatek\UserManager\Entity
 */
class Acl
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue
     * @var integer
     */
    private $id = null;
    /**
     * @ORM\ManyToOne(targetEntity="Role", inversedBy="acls")
     * @ORM\JoinColumn(name="role_id", referencedColumnName="id")
     * @var Role
     */
    private $role;
    /**
     * @ORM\Column(type="string", length=100)
     * @var string
     */
    private $resource;
    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @var string|null
     */
    private $allowed = null;
    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @var string|null
     */
    private $denied = null;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return Acl
     */
    public function setId(int $id): Acl
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return Role
     */
    public function getRole(): Role
    {
        return $this->role;
    }

    /**
     * @param Role $role
     * @return Acl
     */
    public function setRole(Role $role): Acl
    {
        $this->role = $role;
        return $this;
    }

    /**
     * @return string
     */
    public function getResource(): string
    {
        return $this->resource;
    }

    /**
     * @param string $resource
     * @return Acl
     */
    public function setResource(string $resource): Acl
    {
        $this->resource = $resource;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getAllowed()
    {
        return $this->allowed;
    }

    /**
     * @param array|string|null $allowed
     * @return Acl
     */
    public function setAllowed($allowed): Acl
    {
        if (is_string($allowed)) {
            $this->allowed = $allowed;
        }
        if (is_array($allowed) and count($allowed) > 0) {
            $this->allowed = join(",", $allowed);
        } else {
            $this->allowed = null;
        }
        return $this;
    }

    /**
     * @return array|null
     */
    public function getAllowArray(): array
    {
        return $this->allowed ? explode(",", trim($this->allowed)) : [];
    }

    /**
     * @return string|null
     */
    public function getDenied()
    {
        return $this->denied;
    }

    /**
     * @param array|string|null $denied
     * @return Acl
     */
    public function setDenied($denied): Acl
    {
        if (is_string($denied)) {
            $this->denied = $denied;
        }
        if (is_array($denied) and count($denied) > 0) {
            $this->denied = join(",", $denied);
        } else {
            $this->denied = null;
        }
        return $this;
    }

    /**
     * @return array|null
     */
    public function getDenyArray()
    {
        return $this->denied ? explode(",", trim($this->denied)) : [];
    }

    /**
     * @param string $privilege
     */
    public function allow($privilege)
    {
        $denied = array_flip($this->getDenyArray());
        $allowed = array_flip($this->getAllowArray());

        if (isset($denied[$privilege])) {
            unset($denied[$privilege]);
        }
        $allowed[$privilege] = true;

        if (isset($allowed[""])) {
            unset($allowed[""]);
        }
        if (isset($denied[""])) {
            unset($denied[""]);
        }

        $this->setDenied(array_keys($denied));
        $this->setAllowed(array_keys($allowed));
    }

    /**
     * @param string $privilege
     */
    public function deny($privilege)
    {
        $denied = array_flip($this->getDenyArray());
        $allowed = array_flip($this->getAllowArray());

        if (isset($allowed[$privilege])) {
            unset($allowed[$privilege]);
        }
        $denied[$privilege] = true;

        if (isset($allowed[""])) {
            unset($allowed[""]);
        }
        if (isset($denied[""])) {
            unset($denied[""]);
        }

        $this->setDenied(array_keys($denied));
        $this->setAllowed(array_keys($allowed));
    }
}
