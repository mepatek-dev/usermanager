<?php
declare(strict_types=1);

namespace Mepatek\UserManager\Model;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(
 *   name="`role`",
 *   indexes={
 *     @ORM\Index(name="IDX_deleted", columns={"deleted"}),
 *     @ORM\Index(name="IDX_role", columns={"role"}),
 * })
 *
 * @package Mepatek\UserManager\Model
 */
class Role
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue
     * @var integer|null
     */
    private $id = null;
    /**
     * @ORM\Column(type="string", length=30, unique=true)
     * @var string
     */
    private $role;
    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     * @var string|null
     */
    private $name;
    /**
     * @ORM\Column(type="text", nullable=true)
     * @var string|null
     */
    private $description;
    /**
     * @ORM\Column(type="boolean")
     * @var boolean
     */
    private $deleted = false;
    /**
     * Many Roles have Many Child Roles.
     * @ORM\ManyToMany(targetEntity="Role", mappedBy="parentRoles")
     * @var Role[]|Collection
     */
    private $childRoles;
    /**
     * Many Roles have Many Parent Roles.
     * @ORM\ManyToMany(targetEntity="Role", inversedBy="childRoles")
     * @ORM\JoinTable(name="parent_roles",
     *      joinColumns={@ORM\JoinColumn(name="parent_role_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="child_role_id", referencedColumnName="id")}
     *      )
     * @var Role[]|Collection
     */
    private $parentRoles;
    /**
     * Many Roles have Many Users.
     * @ORM\ManyToMany(targetEntity="User", mappedBy="roles")
     * @var User[]|Collection
     */
    private $users;
    /**
     * One Role have Many Acls.
     * @ORM\OneToMany(targetEntity="Acl", mappedBy="role", cascade={"persist", "remove"})
     * @var Acl[]|Collection
     */
    private $acls;


    /**
     * Role constructor.
     */
    public function __construct()
    {
        $this->users = new ArrayCollection();
        $this->childRoles = new ArrayCollection();
        $this->parentRoles = new ArrayCollection();
        $this->acls = new ArrayCollection();
    }

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @param int|null $id
     * @return Role
     */
    public function setId(?int $id): Role
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string
     */
    public function getRole(): string
    {
        return $this->role;
    }

    /**
     * @param string $role
     * @return Role
     */
    public function setRole(string $role): Role
    {
        $this->role = $role;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * @param string|null $name
     * @return Role
     */
    public function setName(?string $name): Role
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getDescription(): ?string
    {
        return $this->description;
    }

    /**
     * @param string|null $description
     * @return Role
     */
    public function setDescription(?string $description): Role
    {
        $this->description = $description;
        return $this;
    }

    /**
     * @return bool
     */
    public function isDeleted(): bool
    {
        return $this->deleted;
    }

    /**
     * @param bool $deleted
     * @return Role
     */
    public function setDeleted(bool $deleted): Role
    {
        $this->deleted = $deleted;
        return $this;
    }

    /**
     * @return Collection|Roles[]
     */
    public function getChildRoles()
    {
        return $this->childRoles;
    }

    /**
     * @return Collection|Role[]
     */
    public function getParentRoles()
    {
        return $this->parentRoles;
    }

    /**
     * @param Collection|Role[] $parentRoles
     * @return Role
     */
    public function setParentRoles($parentRoles)
    {
        if (is_array($parentRoles)) {
            $this->parentRoles->clear();
            foreach ($parentRoles as $parentRole) {
                $this->parentRoles->add($parentRole);
            }
        } elseif ($parentRoles instanceof Collection) {
            $this->parentRoles = $parentRoles;
        }
        return $this;
    }

    /**
     * @return array
     */
    public function getParentRolesArray(): array
    {
        $parentRoles = [];
        foreach ($this->parentRoles as $parentRole) {
            $parentRoles[] = $parentRole->getRole();
        }
        return $parentRoles;
    }

    /**
     * @return array
     */
    public function getParentRolesId(): array
    {
        $parentRolesId = [];
        foreach ($this->parentRoles as $parentRole) {
            $parentRolesId[] = $parentRole->getId();
        }
        return $parentRolesId;
    }

    /**
     * @return Collection|User[]
     */
    public function getUsers(): Collection
    {
        return $this->users;
    }

    /**
     * @param Collection|User[] $users
     * @return Role
     */
    public function setUsers(Collection $users): Role
    {
        $this->users = $users;
        return $this;
    }
}
