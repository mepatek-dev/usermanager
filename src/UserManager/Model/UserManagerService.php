<?php
declare(strict_types=1);

namespace Mepatek\UserManager\Model;

use Doctrine\ORM\EntityManagerInterface;
use Mepatek\UserManager\Model\Facade\AclFacade;
use Mepatek\UserManager\Model\Facade\ResourceFacade;
use Mepatek\UserManager\Model\Facade\RoleFacade;
use Mepatek\UserManager\Model\Facade\UserFacade;
use Nette\Caching\Cache;
use Nette\Caching\IStorage;
use Psr\Log\LoggerAwareInterface;
use Psr\Log\LoggerAwareTrait;
use Psr\Log\LoggerInterface;
use Psr\Log\LoggerTrait;

/**
 * Class UserManagerService
 * @package Mepatek\UserManager\Model
 */
class UserManagerService implements LoggerAwareInterface
{
    use LoggerAwareTrait;
    use LoggerTrait;
    /** @var EntityManagerInterface */
    private $entityManager;
    /** @var IStorage */
    private $storage;
    /** @var string */
    private $resourceNeon;
    /** @var UserFacade */
    private $userFacade = null;
    /** @var RoleFacade */
    private $roleFacade = null;
    /** @var ResourceFacade */
    private $resourceFacade = null;
    /** @var AclFacade */
    private $aclFacade = null;

    /**
     * UserManagerService constructor.
     * @param string $resourceNeon
     * @param EntityManagerInterface $entityManager
     * @param IStorage $storage
     * @param LoggerInterface $logger
     */
    public function __construct(
        string $resourceNeon,
        EntityManagerInterface $entityManager,
        IStorage $storage,
        LoggerInterface $logger
    ) {
        $this->resourceNeon = $resourceNeon;
        $this->entityManager = $entityManager;
        $this->storage = $storage;
        $this->setLogger($logger);
    }

    /**
     * @return UserFacade
     */
    public function getUserFacade(): UserFacade
    {
        if ($this->userFacade === null) {
            $this->userFacade = new UserFacade();
            $this->userFacade->injectEntityManager($this->entityManager);
            $this->userFacade->setLogger($this->logger);
        }
        return $this->userFacade;
    }

    /**
     * @return RoleFacade
     */
    public function getRoleFacade(): RoleFacade
    {
        if ($this->roleFacade === null) {
            $this->roleFacade = new RoleFacade();
            $this->roleFacade->injectEntityManager($this->entityManager);
            $this->roleFacade->setCache($this->getCache());
            $this->roleFacade->setLogger($this->logger);
        }
        return $this->roleFacade;
    }

    /**
     * @return ResourceFacade
     */
    public function getResourceFacade(): ResourceFacade
    {
        if ($this->resourceFacade === null) {
            $this->resourceFacade = new ResourceFacade();
            $this->resourceFacade->setCache($this->getCache());
            $this->resourceFacade->setLogger($this->logger);
            $this->readResourcesFromNeon($this->resourceNeon);
        }
        return $this->resourceFacade;
    }

    /**
     * @param string $neonFile
     */
    public function readResourcesFromNeon(string $neonFile): void
    {
        $this->getResourceFacade()->readResourcesFromNeon($neonFile);
    }

    /**
     * @return AclFacade
     */
    public function getAclFacade(): AclFacade
    {
        if ($this->aclFacade === null) {
            $this->aclFacade = new AclFacade();
            $this->aclFacade->injectEntityManager($this->entityManager);
            $this->aclFacade->setCache($this->getCache());
            $this->aclFacade->setLogger($this->logger);
            $this->aclFacade->setResourceFacade($this->getResourceFacade());
            $roles = $this->getRoleFacade()->getCachedRoles();
            $adminRole = null;
            foreach ($roles as $role) {
                if ($role->getRole()==="admin") {
                    $adminRole = $role;
                    break;
                }
            }
            $this->aclFacade->setAdminRole($adminRole);
        }
        return $this->aclFacade;
    }

    /**
     * @return Cache
     */
    private function getCache(): Cache
    {
        static $cache = null;
        if (!$cache) {
            $cache = new Cache($this->storage, "Usermanager");
        }
        return $cache;
    }

    /**
     * @param $level
     * @param $message
     * @param array $context
     */
    public function log($level, $message, array $context = [])
    {
        $this->logger->log($level, $message, $context);
    }
}
