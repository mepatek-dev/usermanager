<?php
declare(strict_types=1);

namespace Mepatek\UserManager\Model;

use DateTime;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(
 *   indexes={
 *     @ORM\Index(name="IDX_type", columns={"type"}),
 * })
 *
 * @package Mepatek\UserManager\Model
 */
class UserActivity
{
    public const TYPE_CREATE = "create";
    public const TYPE_LOGIN = "login";
    public const TYPE_LOGOUT = "logout";
    public const TYPE_PWTOKEN_GEN = "genPwToken";
    public const TYPE_PWTOKEN_CHANGE_PASSWORD = "chgPwToken";
    public const TYPE_CHANGE_PASSWORD = "chgPass";
    public const TYPE_DELETE = "delete";
    public const TYPE_UNDELETE = "undelete";
    public const TYPE_DISABLE = "sisable";
    public const TYPE_ENABLE = "enable";
    public const TYPE_INVITE = "invite";
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue
     * @var integer
     */
    private $id;
    /**
     * @ORM\ManyToOne(targetEntity="User", inversedBy="activities")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     * @var User
     */
    private $user;
    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     * @var string|null
     */
    private $ip;
    /**
     * @ORM\Column(type="string", length=30)
     * @var string
     */
    private $type;
    /**
     * @ORM\Column(type="datetime")
     * @var DateTime
     */
    private $datetime;
    /**
     * @ORM\Column(type="text", nullable=true)
     * @var string|null
     */
    private $description;

    /**
     * UserActivity constructor.
     */
    public function __construct()
    {
        $this->datetime = new DateTime();
        $this->ip = self::getRemoteIp();
    }

    /**
     * @return string
     */
    public static function getRemoteIp(): string
    {
        return getenv('HTTP_CLIENT_IP')
            ?: getenv('HTTP_X_FORWARDED_FOR')
            ?: getenv('HTTP_X_FORWARDED')
                ?: getenv('HTTP_FORWARDED_FOR')
                    ?: getenv('HTTP_FORWARDED') ?: getenv('REMOTE_ADDR');
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return UserActivity
     */
    public function setId(int $id): UserActivity
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return User
     */
    public function getUser(): User
    {
        return $this->user;
    }

    /**
     * @param User $user
     * @return UserActivity
     */
    public function setUser(User $user): UserActivity
    {
        $this->user = $user;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getIp(): ?string
    {
        return $this->ip;
    }

    /**
     * @param string|null $ip
     * @return UserActivity
     */
    public function setIp(?string $ip): UserActivity
    {
        $this->ip = $ip;
        return $this;
    }

    /**
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }

    /**
     * @param string $type
     * @return UserActivity
     */
    public function setType(string $type): UserActivity
    {
        $this->type = $type;
        return $this;
    }

    /**
     * @return DateTime
     */
    public function getDatetime(): DateTime
    {
        return $this->datetime;
    }

    /**
     * @param DateTime $datetime
     * @return UserActivity
     */
    public function setDatetime(DateTime $datetime): UserActivity
    {
        $this->datetime = $datetime;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getDescription(): ?string
    {
        return $this->description;
    }

    /**
     * @param string|null $description
     * @return UserActivity
     */
    public function setDescription(?string $description): UserActivity
    {
        $this->description = $description;
        return $this;
    }
}
