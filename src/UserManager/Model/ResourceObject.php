<?php
declare(strict_types=1);

namespace Mepatek\UserManager\Model;

/**
 * Class ResourceObject
 * @package Mepatek\UserManager\Model
 */
class ResourceObject
{
    /**
     * @var string
     */
    protected $resource = null;
    /**
     * @var string
     */
    protected $title;
    /**
     * @var string|null
     */
    protected $description = null;
    /**
     * @var array privilege=>description
     */
    protected $privileges = [];

    /**
     * @return string
     */
    public function getResource(): string
    {
        return $this->resource;
    }

    /**
     * @param string $resource
     * @return ResourceObject
     */
    public function setResource(string $resource): ResourceObject
    {
        $this->resource = $resource;
        return $this;
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @param string $title
     * @return ResourceObject
     */
    public function setTitle(string $title): ResourceObject
    {
        $this->title = $title;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getDescription(): ?string
    {
        return $this->description;
    }

    /**
     * @param string|null $description
     * @return ResourceObject
     */
    public function setDescription(?string $description): ResourceObject
    {
        $this->description = $description;
        return $this;
    }

    /**
     * @return array
     */
    public function getPrivileges(): array
    {
        return $this->privileges;
    }

    /**
     * @param array $privileges
     * @return ResourceObject
     */
    public function setPrivileges(array $privileges): ResourceObject
    {
        $this->privileges = $privileges;
        return $this;
    }

    /**
     * Is privilege in privileges?
     *
     * @param string $privilege
     *
     * @return bool
     */
    public function isInPrivileges(string $privilege): bool
    {
        return key_exists($privilege, $this->privileges);
    }
}
