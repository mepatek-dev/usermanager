<?php
declare(strict_types=1);

namespace Mepatek\UserManager\Model;

use DateTime;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Nette\Utils\Strings;

/**
 * @ORM\Entity
 * @ORM\Table(
 *   name="`user`",
 *   indexes={
 *     @ORM\Index(name="IDX_deleted", columns={"deleted"}),
 *     @ORM\Index(name="IDX_user_name", columns={"user_name"}),
 *     @ORM\Index(name="IDX_email", columns={"email"}),
 *     @ORM\Index(name="IDX_disabled", columns={"disabled"}),
 * })
 *
 * @package Mepatek\UserManager\Model
 */
class User
{
    const PROPERTIES_FOR_IDENTITY = "fullName,userName,email,phone,title,language,thumbnail";
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue
     * @var integer|null
     */
    private $id = null;
    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @var string|null
     */
    private $fullName;
    /**
     * @ORM\Column(type="string", length=50, unique=true)
     * @var string
     */
    private $userName = "";
    /**
     * @ORM\Column(type="string", length=200)
     * @var string
     */
    private $pwHash = "";
    /**
     * @ORM\Column(type="string", length=255, unique=true)
     * @var string
     */
    private $email = "";
    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @var string|null
     */
    private $phone;
    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     * @var string|null
     */
    private $title;
    /**
     * @ORM\Column(type="string", length=10, nullable=true)
     * @var string|null
     */
    private $language;
    /**
     * @ORM\Column(type="blob", nullable=true)
     * encoded image for atribute src
     *
     * @var string|null
     */
    private $thumbnail;
    /**
     * @ORM\Column(type="string", length=255, nullable=true, unique=true)
     * @var string|null
     */
    private $pwToken;
    /**
     * @ORM\Column(type="datetime", nullable=true)
     * @var DateTime|null
     */
    private $pwTokenExpire;
    /**
     * @ORM\Column(type="datetime", nullable=true)
     * @var DateTime
     */
    private $created;
    /**
     * @ORM\Column(type="datetime", nullable=true)
     * @var DateTime|null
     */
    private $lastLogged;
    /**
     * @ORM\Column(type="boolean", nullable=true)
     * @var boolean
     */
    private $invited = false;
    /**
     * @ORM\Column(type="boolean", nullable=true)
     * @var boolean
     */
    private $disabled = false;
    /**
     * @ORM\Column(type="boolean")
     * @var boolean
     */
    private $deleted = false;
    /**
     * @ORM\ManyToMany(targetEntity="Role", inversedBy="users", cascade={"persist", "remove"})
     * @ORM\JoinTable(
     *      joinColumns={@ORM\JoinColumn(name="user_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="role_id", referencedColumnName="id")}
     *      )
     * @var Role[]|Collection
     */
    private $roles;
    /**
     * @ORM\OneToMany(targetEntity="AuthDriver", mappedBy="user", cascade={"persist", "remove"})
     * @var AuthDriver[]|Collection
     */
    private $authDrivers = [];
    /**
     * @ORM\OneToMany(targetEntity="UserActivity", mappedBy="user", cascade={"persist", "remove"})
     * @var UserActivity[]|Collection
     */
    private $activities = [];

    /**
     * User constructor.
     */
    public function __construct()
    {
        $this->roles = new ArrayCollection();
        $this->authDrivers = new ArrayCollection();
        $this->created = new DateTime();
    }

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @param int|null $id
     * @return User
     */
    public function setId(?int $id): User
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getFullName(): ?string
    {
        return $this->fullName;
    }

    /**
     * @param string|null $fullName
     * @return User
     */
    public function setFullName(?string $fullName): User
    {
        $this->fullName = $fullName;
        return $this;
    }

    /**
     * @return string
     */
    public function getUserName(): string
    {
        return $this->userName;
    }

    /**
     * @param string $userName
     * @return User
     */
    public function setUserName(string $userName): User
    {
        $this->userName = $userName;
        return $this;
    }

    /**
     * @return string
     */
    public function getEmail(): string
    {
        return $this->email;
    }

    /**
     * @param string $email
     * @return User
     */
    public function setEmail(string $email): User
    {
        $this->email = $email;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getPhone(): ?string
    {
        return $this->phone;
    }

    /**
     * @param string|null $phone
     * @return User
     */
    public function setPhone(?string $phone): User
    {
        $this->phone = $phone;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getTitle(): ?string
    {
        return $this->title;
    }

    /**
     * @param string|null $title
     * @return User
     */
    public function setTitle(?string $title): User
    {
        $this->title = $title;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getLanguage(): ?string
    {
        return $this->language;
    }

    /**
     * @param string|null $language
     * @return User
     */
    public function setLanguage(?string $language): User
    {
        $this->language = $language;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getPwToken(): ?string
    {
        return $this->pwToken;
    }

    /**
     * @param string|null $pwToken
     * @return User
     */
    public function setPwToken(?string $pwToken): User
    {
        $this->pwToken = $pwToken;
        return $this;
    }

    /**
     * @return DateTime|null
     */
    public function getPwTokenExpire(): ?DateTime
    {
        return $this->pwTokenExpire;
    }

    /**
     * @param DateTime|null $pwTokenExpire
     * @return User
     */
    public function setPwTokenExpire(?DateTime $pwTokenExpire): User
    {
        $this->pwTokenExpire = $pwTokenExpire;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getThumbnail(): ?string
    {
        if (is_resource($this->thumbnail)) {
            return stream_get_contents($this->thumbnail);
        }
        return $this->thumbnail;
    }

    /**
     * @param string|null $thumbnail
     * @return User
     */
    public function setThumbnail(?string $thumbnail): User
    {
        $this->thumbnail = $thumbnail;
        return $this;
    }

    /**
     * @return boolean
     */
    public function hasThumbnail(): bool
    {
        return (bool)$this->thumbnail;
    }

    /**
     * @return string
     */
    public function getPwHash(): string
    {
        return $this->pwHash;
    }

    /**
     * @param string $pwHash
     * @return User
     */
    public function setPwHash(string $pwHash): User
    {
        $this->pwHash = $pwHash;
        return $this;
    }

    /**
     * @return DateTime
     */
    public function getCreated(): DateTime
    {
        return $this->created;
    }

    /**
     * @param DateTime $created
     * @return User
     */
    public function setCreated(DateTime $created): User
    {
        $this->created = $created;
        return $this;
    }

    /**
     * @return DateTime|null
     */
    public function getLastLogged(): ?DateTime
    {
        return $this->lastLogged;
    }

    /**
     * @param DateTime|null $lastLogged
     * @return User
     */
    public function setLastLogged(?DateTime $lastLogged): User
    {
        $this->lastLogged = $lastLogged;
        return $this;
    }

    /**
     * @return bool
     */
    public function isInvited(): bool
    {
        return $this->invited;
    }

    /**
     * @param bool $invited
     * @return User
     */
    public function setInvited(bool $invited = true): User
    {
        $this->invited = $invited;
        return $this;
    }

    /**
     * @return bool
     */
    public function isDisabled(): bool
    {
        return $this->disabled;
    }

    /**
     * @param bool $disabled
     * @return User
     */
    public function setDisabled(bool $disabled = true): User
    {
        $this->disabled = $disabled;
        return $this;
    }

    /**
     * @return bool
     */
    public function isDeleted(): bool
    {
        return $this->deleted;
    }

    /**
     * @param bool $deleted
     * @return User
     */
    public function setDeleted(bool $deleted = true): User
    {
        $this->deleted = $deleted;
        return $this;
    }

    /**
     * @return Role[]|Collection
     */
    public function getRoles(): Collection
    {
        return $this->roles;
    }

    /**
     * @param Role[]|Collection $roles
     * @return User
     */
    public function setRoles(Collection $roles): User
    {
        $this->roles = $roles;
        return $this;
    }

    /**
     * @return User
     */
    public function resetRoles(): User
    {
        $this->roles = new ArrayCollection();
        return $this;
    }

    /**
     * @return AuthDriver[]|Collection
     */
    public function getAuthDrivers(): Collection
    {
        return $this->authDrivers;
    }

    /**
     * @param AuthDriver[]|Collection $authDrivers
     * @return User
     */
    public function setAuthDrivers(Collection $authDrivers): User
    {
        $this->authDrivers = $authDrivers;
        return $this;
    }

    /**
     * @return UserActivity[]|Collection
     */
    public function getActivities(): Collection
    {
        return $this->activities;
    }

    /**
     * @param UserActivity[]|Collection $activities
     * @return User
     */
    public function setActivities(Collection $activities): User
    {
        $this->activities = $activities;
        return $this;
    }

    /**
     * @param Role $role
     * @return User
     */
    public function addRole(Role $role): User
    {
        $this->roles[] = $role;
        return $this;
    }

    /**
     * @param AuthDriver $authDriver
     * @return User
     */
    public function addAuthDriver(AuthDriver $authDriver): User
    {
        if (($foundAuthDriver = $this->getAuthDriverByName($authDriver->getAuthDriver()))) {
            $foundAuthDriver->setAuthId($authDriver->getAuthId());
        } else {
            $authDriver->setUser($this);
            $this->authDrivers->add($authDriver);
        }
        return $this;
    }

    /**
     * @param string $type
     * @param string|null $description
     * @return User
     */
    public function addActivity(string $type, ?string $description = null): User
    {
        $userActivity = new UserActivity();
        $userActivity->setType($type);
        $userActivity->setUser($this);
        if ($description) {
            $userActivity->setDescription($description);
        }
        $this->activities[] = $userActivity;
        return $this;
    }

    /**
     * @param string $name
     *
     * @return AuthDriver|null
     */
    public function getAuthDriverByName(string $name): ?AuthDriver
    {
        $foundAuthDriver = null;
        foreach ($this->authDrivers as $authDriver) {
            if ($authDriver->getAuthDriver() == $name) {
                $foundAuthDriver = $authDriver;
                break;
            }
        }
        return $foundAuthDriver;
    }

    /**
     * @param string $role
     *
     * @return bool
     */
    public function isInRole($role): bool
    {
        foreach ($this->roles as $r) {
            if ($r->getRole() == $role) {
                return true;
            }
        }
        return false;
    }

    /**
     * @return array
     */
    public function getIdentityRoles(): array
    {
        $roles = [];
        foreach ($this->roles as $role) {
            $roles[] = $role->getRole();
        }
        return $roles;
    }

    /**
     * @return array
     */
    public function getIdentityData()
    {
        $identityData = [];
        foreach (explode(",", self::PROPERTIES_FOR_IDENTITY) as $property) {
            $getter = "get" . Strings::firstUpper($property);
            $identityData[$property] = $this->$getter();
        }
        return $identityData;
    }
}
