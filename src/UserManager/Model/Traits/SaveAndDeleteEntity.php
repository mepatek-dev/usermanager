<?php
declare(strict_types=1);

namespace Mepatek\UserManager\Model\Traits;

use Doctrine\ORM\EntityManager;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Doctrine\ORM\UnitOfWork;

/**
 * Trait SaveAndDeleteEntity
 * @package App\Model\Common\Traits
 */
trait SaveAndDeleteEntity
{
    /** @var EntityManager */
    protected $entityManager;

    /**
     * Save doctrine Entity by state
     * @param mixed $entity
     * @throws ORMException
     * @throws OptimisticLockException
     */
    protected function saveEntity($entity): void
    {
        $state = $this->entityManager
            ->getUnitOfWork()
            ->getEntityState($entity);
        switch ($state) {
            case UnitOfWork::STATE_NEW:
                $this->entityManager->persist($entity);
                break;
            case UnitOfWork::STATE_DETACHED:
                $this->entityManager->merge($entity);
                break;
        }
        $this->entityManager->flush();
    }

    /**
     * Delete Doctrine Entity
     *
     * @param mixed $entity
     * @throws ORMException
     * @throws OptimisticLockException
     */
    protected function deleteEntity($entity): void
    {
        $this->entityManager->remove($entity);
        $this->entityManager->flush();
    }
}
