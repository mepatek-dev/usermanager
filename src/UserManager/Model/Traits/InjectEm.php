<?php
declare(strict_types=1);

namespace Mepatek\UserManager\Model\Traits;

use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\QueryBuilder;
use Nette\Utils\Strings;

/**
 * Trait InjectEm
 * @package App\Model\Commons\Traits
 */
trait InjectEm
{
    /** @var EntityManager */
    protected $entityManager;

    /**
     * Inject EM
     * @param EntityManagerInterface $entityManager
     */
    public function injectEntityManager(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * Get QueryBuilder for Entity
     *
     * @param string $entityClass
     * @param string|null $qbName
     * @return QueryBuilder
     */
    protected function getEntityQB(string $entityClass, ?string $qbName = null): QueryBuilder
    {
        if ($qbName === null) {
            $path = explode('\\', $entityClass);
            $qbName = Strings::firstLower(array_pop($path));
        }
        return $this->entityManager->getRepository($entityClass)->createQueryBuilder($qbName);
    }
}
