<?php
declare(strict_types=1);

namespace Mepatek\UserManager\Model\Facade;

use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Doctrine\ORM\QueryBuilder;
use Mepatek\UserManager\Model\Acl;
use Mepatek\UserManager\Model\ResourceObject;
use Mepatek\UserManager\Model\Role;
use Mepatek\UserManager\Model\Traits\InjectEm;
use Mepatek\UserManager\Model\Traits\SaveAndDeleteEntity;
use Nette\Caching\Cache;
use Psr\Log\LoggerAwareInterface;
use Psr\Log\LoggerAwareTrait;

/**
 * Class AclFacade
 * @package Mepatek\UserManager\Model\Facade
 */
class AclFacade implements LoggerAwareInterface
{
    use InjectEm;
    use SaveAndDeleteEntity;
    use LoggerAwareTrait;
    const CACHE_TAG_ACLS_LIST = "AclsList";
    /** @var ResourceFacade */
    private $resourceFacade;
    /** @var Cache */
    private $cache;
    /** @var Role */
    private $adminRole;

    /**
     * Get cached list of ACL
     *
     * @return Acl[]
     */
    public function getCachedAcls(): array
    {
        $acls = null;
        if ($this->cache) {
            $acls = $this->cache->load("AclList");
        }
        if ($acls == null) {
            $acls = $this->getAcls();
            $this->cache->save(
                "AclList",
                $acls,
                [
                    Cache::EXPIRE => "1 month",
                    Cache::SLIDING => true,
                    Cache::TAGS => [
                        self::CACHE_TAG_ACLS_LIST,
                        RoleFacade::CACHE_TAG_ROLES_LIST,
                    ],
                ]
            );
        }
        return $acls;
    }

    /**
     * Get list of acls
     * @return Acl[]
     */
    public function getAcls(): array
    {
        /** @var Acl[] $acls */
        $acls = [];
        $qb = $this->getAclQB();
        /** @var Acl[] $aclsDb */
        $aclsDb = $qb->getQuery()->getResult();

        /** @var ResourceObject[] $resources */
        $resources = $this->resourceFacade->getResources();
        $adminSet = false;
        foreach ($aclsDb as $acl) {
            $acls[] = $acl;
            if ($acl->getRole()->getRole() == $this->adminRole->getRole()) {
                $adminSet = true;
            }
        }
        // set admin privileges if not set
        if (!$adminSet) {
            foreach ($resources as $resource) {
                $acl = new Acl();

                $acl->setRole($this->adminRole);
                $acl->setResource($resource->getResource());
                $allow = [];
                foreach ($resource->getPrivileges() as $privilege => $privilegeTitle) {
                    $allow[] = $privilege;
                }
                $acl->setAllowed($allow);
                $acls[] = $acl;
            }
        }

        return $acls;
    }

    /**
     * Find ACL by id
     *
     * @param int $id
     * @return Acl|null
     */
    public function find(int $id): ?Acl
    {
        $qb = $this->getAclQB()
            ->andWhere("acl.id=:id")
            ->setParameter("id", $id);
        /** @var Acl $acl */
        try {
            $acl = $qb->getQuery()->getOneOrNullResult();
        } catch (NonUniqueResultException $e) {
            $this->logger->critical(
                "AclFacade: find - not unique ID. " .
                $e->getMessage()
            );
            return null;
        }
        return $acl;
    }

    /**
     * Find ACL by role and resourec
     *
     * @param Role $role
     * @param string $resource
     * @return Acl|null
     */
    public function findByRoleAndResource(Role $role, string $resource): ?Acl
    {
        $qb = $this->getAclQB()
            ->andWhere("acl.role=:role")
            ->andWhere("acl.resource=:resource")
            ->setParameter("role", $role)
            ->setParameter("resource", $resource);
        /** @var Acl $acl */
        try {
            $acl = $qb->getQuery()->getOneOrNullResult();
        } catch (NonUniqueResultException $e) {
            $this->logger->error(
                "AclFacade: findByRoleAndResource - not unique role and resource. " .
                $e->getMessage()
            );
            return null;
        }
        return $acl;
    }

    /**
     * Get QueryBuilder for acls
     *
     * @return QueryBuilder
     */
    public function getAclQB(bool $deleted = false): QueryBuilder
    {
        $qb = $this->getEntityQB(Acl::class)
            ->addSelect("role")
            ->innerJoin("acl.role", "role");
        if (!$deleted) {
            $qb->andWhere("role.deleted = false");
        }
        return $qb;
    }

    /**
     * Save acl, clear cache
     *
     * @param Acl $acl
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function saveAcl(Acl $acl): void
    {
        $this->saveEntity($acl);
        $this->clearCache(self::CACHE_TAG_ACLS_LIST);
    }

    /**
     * Delete acl, clear cache
     *
     * @param Acl $acl
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function deleteAcl(Acl $acl): void
    {
        $this->deleteEntity($acl);
        $this->clearCache(self::CACHE_TAG_ACLS_LIST);
    }

    /**
     * Get role for admin
     *
     * @return Role
     */
    public function getAdminRole(): Role
    {
        return $this->adminRole;
    }

    /**
     * Set role for admin
     *
     * @param Role $adminRole
     * @return AclFacade
     */
    public function setAdminRole(Role $adminRole): AclFacade
    {
        $this->adminRole = $adminRole;
        return $this;
    }

    /**
     * @param Cache $cache
     * @return AclFacade
     */
    public function setCache(Cache $cache): AclFacade
    {
        $this->cache = $cache;
        return $this;
    }

    /**
     * @return ResourceFacade
     */
    public function getResourceFacade(): ResourceFacade
    {
        return $this->resourceFacade;
    }

    /**
     * @param ResourceFacade $resourceFacade
     * @return AclFacade
     */
    public function setResourceFacade(ResourceFacade $resourceFacade): AclFacade
    {
        $this->resourceFacade = $resourceFacade;
        return $this;
    }

    /**
     * Clear cache
     *
     * @param null|string|array $tags null (all) or tags
     */
    public function clearCache($tags = null): void
    {
        if ($this->cache) {
            if (is_string($tags)) {
                $clean = [
                    CACHE::TAGS => [$tags],
                ];
            } elseif (is_array($tags)) {
                $clean = [
                    CACHE::TAGS => $tags,
                ];
            } else {
                $clean = [
                    CACHE::ALL => true,
                ];
            }
            $this->cache->clean($clean);
        }
    }
}
