<?php
declare(strict_types=1);

namespace Mepatek\UserManager\Model\Facade;

use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Doctrine\ORM\QueryBuilder;
use Mepatek\UserManager\Model\Role;
use Mepatek\UserManager\Model\Traits\InjectEm;
use Mepatek\UserManager\Model\Traits\SaveAndDeleteEntity;
use Nette\Caching\Cache;
use Psr\Log\LoggerAwareInterface;
use Psr\Log\LoggerAwareTrait;

/**
 * Class RoleFacade
 * @package Mepatek\UserManager\Model\Facade
 */
class RoleFacade implements LoggerAwareInterface
{
    use InjectEm;
    use SaveAndDeleteEntity;
    use LoggerAwareTrait;
    const CACHE_TAG_ROLES_LIST = "RolesList";
    /** @var Cache */
    private $cache = null;

    /**
     * List of roles with builtIn
     *
     * @return Role[]
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function getRoles(): array
    {
        $roles = [];
        /** @var Role[] $rolesDb */
        $rolesDb = $this->getRoleQB()
            ->getQuery()
            ->getResult();

        $builtInRoles = $this->getBuiltInRoles();
        foreach ($rolesDb as $role) {
            $key = $role->getRole();
            if (isset($builtInRoles[$key])) {
                unset($builtInRoles[$key]);
            }
            $roles[] = $role;
        }
        foreach ($builtInRoles as $role) {
            $this->saveRole($role);
            $roles[] = $role;
        }

        return $roles;
    }

    /**
     * Find Role by id
     *
     * @param int $id
     * @return null|Role
     */
    public function find(int $id): ?Role
    {
        $qb = $this->getRoleQB(true, true)
            ->andWhere("role.id=:id")
            ->setParameter("id", $id);
        /** @var Role $role */
        try {
            $role = $qb->getQuery()
                ->getOneOrNullResult();
        } catch (NonUniqueResultException $e) {
            $this->logger->critical(
                "RoleFacade: find - not unique ID. " .
                $e->getMessage()
            );
            return null;
        }
        return $role;
    }

    /**
     * Find role by role name
     *
     * @param string $roleName
     * @return null|Role
     */
    public function findByRole(string $roleName): ?Role
    {
        $qb = $this->getRoleQB()
            ->andWhere("role.role=:role")
            ->setParameter("role", $roleName);
        /** @var Role $role */
        try {
            $role = $qb->getQuery()
                ->getOneOrNullResult();
        } catch (NonUniqueResultException $e) {
            $this->logger->critical(
                "RoleFacade: findByRole - not unique role name. " .
                $e->getMessage()
            );
            return null;
        }
        return $role;
    }

    /**
     * Save role, clear cache
     *
     * @param Role $role
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function saveRole(Role $role)
    {
        $this->saveEntity($role);
        $this->clearCache(self::CACHE_TAG_ROLES_LIST);
    }

    /**
     * Delete role (deleted), clear cache
     *
     * @param Role $role
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function deleteRole(Role $role)
    {
        $role->setDeleted(true);
        $this->saveRole($role);
    }

    /**
     * Permanent delete role (deleted), clear cache
     *
     * @param Role $role
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function permanentDeleteRole(Role $role)
    {
        $this->deleteEntity($role);
        $this->clearCache(self::CACHE_TAG_ROLES_LIST);
    }

    /**
     * 
     * Get QueryBuilder for roles
     * @param bool $fillAll
     * @return QueryBuilder
     */
    public function getRoleQB(bool $fillAll = true, bool $deleted = false): QueryBuilder
    {
        $qb = $this->getEntityQB(Role::class);
        if ($fillAll) {
            $qb->addSelect("user")
                ->leftJoin("role.users", "user");
        }
        if (!$deleted) {
            $qb->andWhere("role.deleted = false");
        }
        return $qb;
    }

    /**
     * @return Cache
     */
    public function getCache(): Cache
    {
        return $this->cache;
    }

    /**
     * @param Cache $cache
     * @return RoleFacade
     */
    public function setCache(Cache $cache): RoleFacade
    {
        $this->cache = $cache;
        return $this;
    }

    /**
     * Cached list of roles
     *
     * @return Role[]
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function getCachedRoles(): array
    {
        if ($this->cache) {
            $roles = $this->cache->load("RoleList");
            if (!is_array($roles)) {
                $roles = $this->getRoles();
                $this->cache->save(
                    "RoleList",
                    $roles,
                    [
                        Cache::EXPIRE => "1 month",
                        Cache::SLIDING => true,
                        Cache::TAGS => [
                            self::CACHE_TAG_ROLES_LIST,
                        ],
                    ]
                );
            }
        } else {
            $roles = $this->getRoles();
        }
        return $roles;
    }

    /**
     * Clear cache
     *
     * @param null|string|array $tags null (all) or tags
     */
    public function clearCache($tags = null): void
    {
        if ($this->cache) {
            if (is_string($tags)) {
                $clean = [
                    CACHE::TAGS => [$tags],
                ];
            } elseif (is_array($tags)) {
                $clean = [
                    CACHE::TAGS => $tags,
                ];
            } else {
                $clean = [
                    CACHE::ALL => true,
                ];
            }
            $this->cache->clean($clean);
        }
    }

    /**
     * Get BuildIn Roles
     *
     * @return Role[]
     */
    protected function getBuiltInRoles(): array
    {
        $builtInRoles = [
            "admin" => "Administrators",    // special role for not logged in users
            "guest" => "Guests",    // special role for admin -> all privileges if not set in acl
            "authenticated" => "Authenticated users",    // special role for user without any role
        ];

        $roles = [];
        foreach ($builtInRoles as $key => $bir) {
            $builtInRole = new Role();
            $builtInRole->setRole($key);
            $builtInRole->setDescription($bir);
            $roles[$key] = $builtInRole;
        }
        return $roles;
    }
}
