<?php
declare(strict_types=1);

namespace Mepatek\UserManager\Model\Facade;

use Mepatek\UserManager\Model\ResourceObject;
use Nette\Caching\Cache;
use Nette\Neon\Neon;
use Psr\Log\LoggerAwareInterface;
use Psr\Log\LoggerAwareTrait;

/**
 * Class ResourceFacade
 * @package Mepatek\UserManager\Model\Facade
 */
class ResourceFacade implements LoggerAwareInterface
{
    use LoggerAwareTrait;
    /** @var Cache */
    protected $cache = null;
    /** @var ResourceObject[] */
    protected $resources = [];

    /**
     * read resources from NEON file
     * format neon file:
     * resource:
     *    title: Title of resource
     *    description: [Description]
     *    privileges:
     *       privilege1: description privilege 1
     *       privilege2: description privilege 2
     *       ...
     * resource2:
     *    ...
     * @param string $neonFile
     * @param bool $useCache
     */
    public function readResourcesFromNeon(string $neonFile, bool $useCache = true): void
    {
        if ($this->cache and $useCache) {
            $this->resources = $this->cache->load($neonFile);
            if (!$this->resources) {
                $this->readResourcesFromNeon($neonFile, false);
                $this->cache->save(
                    $neonFile,
                    $this->resources,
                    [
                        Cache::FILES => $neonFile,
                        Cache::SLIDING => true,
                        Cache::EXPIRE => "1 month",
                    ]
                );
            }
        } else {
            $this->resources = [];
            $result = Neon::decode(file_get_contents($neonFile));
            foreach ($result as $key => $item) {
                $resourceObject = new ResourceObject();
                $resourceObject->setResource($key);
                $resourceObject->setTitle($item["title"]);
                $resourceObject->setPrivileges($item["privileges"]);
                $resourceObject->setDescription($item["description"]);
                $this->resources[] = $resourceObject;
            }
        }
    }

    /**
     * Find ResourceObject by resource name
     *
     * @param string $resource
     * @return ResourceObject|null
     */
    public function find(string $resource): ?ResourceObject
    {
        if (array_key_exists($resource, $this->resources)) {
            return $this->resources[$resource];
        } else {
            return null;
        }
    }

    /**
     * Get all resources as array
     * @return ResourceObject[]
     */
    public function getResources(): array
    {
        return $this->resources;
    }

    /**
     * Set Resources (array)
     *
     * @param ResourceObject[] $resources
     * @return ResourceFacade
     */
    public function setResources(array $resources): ResourceFacade
    {
        $this->resources = $resources;
        return $this;
    }

    /**
     * @param Cache $cache
     * @return ResourceFacade
     */
    public function setCache(Cache $cache): ResourceFacade
    {
        $this->cache = $cache;
        return $this;
    }
}
