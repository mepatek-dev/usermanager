<?php
declare(strict_types=1);

namespace Mepatek\UserManager\Model\Facade;

use DateInterval;
use DateTime;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Doctrine\ORM\QueryBuilder;
use Exception;
use Mepatek\UserManager\Model\Traits\InjectEm;
use Mepatek\UserManager\Model\Traits\SaveAndDeleteEntity;
use Mepatek\UserManager\Model\User;
use Mepatek\UserManager\Model\UserActivity;
use Psr\Log\LoggerAwareInterface;
use Psr\Log\LoggerAwareTrait;

/**
 * Class UserFacade
 * @package Mepatek\UserManager\Model\Facade
 */
class UserFacade implements LoggerAwareInterface
{
    use InjectEm;
    use SaveAndDeleteEntity;
    use LoggerAwareTrait;

    /**
     * Find user by ID
     *
     * @param int $id
     * @param bool $fillAll
     * @return User|null
     */
    public function find(int $id, bool $fillAll = true): ?User
    {
        $qb = $this->getUserQB(true, $fillAll, true)
            ->andWhere("user.id=:id")
            ->setParameter("id", $id);
        /** @var User $user */
        try {
            $user = $qb->getQuery()->getOneOrNullResult();
        } catch (NonUniqueResultException $e) {
            $this->logger->critical(
                "UserFacade: find - not unique ID. " .
                $e->getMessage()
            );
            return null;
        }
        return $user;
    }

    /**
     * Find user by email
     *
     * @param string $email
     * @return User|null
     */
    public function findUserByEmail(string $email, bool $disabled = true, bool $deleted = true): ?User
    {
        // find all users, disabled and deleted (if not changed in parameters)
        $qb = $this->getUserQB($disabled, true, $deleted)
            ->andWhere("user.email=:email")
            ->setParameter("email", $email);
        /** @var User $user */
        try {
            $user = $qb->getQuery()->getOneOrNullResult();
        } catch (NonUniqueResultException $e) {
            $this->logger->critical(
                "UserFacade: findUserByEmail - not unique email. " .
                $e->getMessage()
            );
            return null;
        }
        return $user;
    }

    /**
     * Find user by email
     *
     * @param string $userName
     * @return User|null
     */
    public function findUserByUserName(string $userName): ?User
    {
        $qb = $this->getUserQB(false, true)
            ->andWhere("user.userName=:userName")
            ->setParameter("userName", $userName);
        /** @var User $user */
        try {
            $user = $qb->getQuery()->getOneOrNullResult();
        } catch (NonUniqueResultException $e) {
            $this->logger->critical(
                "UserFacade: findUserByUserName - not unique userName. " .
                $e->getMessage()
            );
            return null;
        }
        return $user;
    }
    /**
     * Find user by email
     *
     * @param string $fullName
     * @return User|null
     */
    public function findUserByFullName(string $fullName): ?User
    {
        $qb = $this->getUserQB(false, true)
            ->andWhere("user.fullName=:fullName")
            ->setParameter("fullName", $fullName);
        /** @var User $user */
        try {
            $user = $qb->getQuery()->getOneOrNullResult();
        } catch (NonUniqueResultException $e) {
            $this->logger->critical(
                "UserFacade: findUserByFullName - not unique userName. " .
                $e->getMessage()
            );
            return null;
        }
        return $user;
    }


    /**
     * Find user by token and pwTokenExpiration
     *
     * @param string $token
     * @return User|null
     */
    public function findUserByPwToken(string $token): ?User
    {
        $qb = $this->getUserQB(false, true)
            ->andWhere("user.pwToken=:token")
            ->andWhere("user.pwTokenExpire>=CURRENT_TIMESTAMP()")
            ->setParameter("token", $token);
        /** @var User $user */
        try {
            $user = $qb->getQuery()->getOneOrNullResult();
        } catch (NonUniqueResultException $e) {
            $this->logger->error(
                "UserFacade: findUserByPwToken - not unique pwToken. " .
                $e->getMessage()
            );
            return null;
        }
        return $user;
    }

    /**
     * Find user by authDriver and AuthId
     *
     * @param string $authDriver
     * @param string $authId
     * @return User|null
     */
    public function findUserByAuthDriverAndAuthId(
        string $authDriver,
        string $authId,
    bool $disabled = false,
    bool $deleted = false
    ): ?User {
        $qb = $this->getUserQB($disabled, true, $deleted)
            ->andWhere("authDriver.authDriver=:authDriver")
            ->andWhere("authDriver.authId=:authId")
            ->setParameter("authDriver", $authDriver)
            ->setParameter("authId", $authId);
        /** @var User $user */
        try {
            $user = $qb->getQuery()->getOneOrNullResult();
        } catch (NonUniqueResultException $e) {
            $this->logger->error(
                "UserFacade: findUserByAuthDriverAndAuthId - not unique authDriver and authId. " .
                $e->getMessage()
            );
            return null;
        }
        return $user;
    }

    /**
     * Get QueryBuilder for users
     *
     * @param bool $disabled
     * @param bool $fillAll
     * @param bool $deleted
     * @return QueryBuilder
     */
    public function getUserQB(bool $disabled = false, bool $fillAll = true, bool $deleted = false): QueryBuilder
    {
        $qb = $this->getEntityQB(User::class);
        if (!$disabled) {
            $qb->andWhere("user.disabled=false");
        }
        if (!$deleted) {
            $qb->andWhere("user.deleted=false");
        }
        if ($fillAll) {
            $qb->addSelect("role")
                ->addSelect("authDriver")
                ->leftJoin("user.roles", "role")
                ->leftJoin("user.authDrivers", "authDriver");
        }
        return $qb;
    }

    /**
     * Get Users list. Array id=>fullName.
     *
     * @param bool $disabled
     * @return array
     */
    public function getUserList(bool $disabled = false): array
    {
        $users = [];
        $qb = $this->getUserQB($disabled, false);
        $qb->orderBy("user.fullName");
        /** @var User $user */
        foreach ($qb->getQuery()->getResult() as $user) {
            $users[$user->getId()] = $user->getFullName();
        }
        return $users;
    }

    /**
     * @param string[] $roles
     * @param bool $disabled
     * @return array
     */
    public function getUserListByRole(array $roles, bool $disabled = false): array
    {
        $users = [];
        $qb = $this->getUserQB($disabled, true);
        $qb
            ->andWhere($qb->expr()->in("role.role", $roles));
        $qb->orderBy("user.fullName");
        /** @var User $user */
        foreach ($qb->getQuery()->getResult() as $user) {
            $users[$user->getId()] = $user->getFullName();
        }
        return $users;
    }

    /**
     * Save user.
     *
     * @param User $user
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function saveUser(User $user): void
    {
        $this->saveEntity($user);
    }

    /**
     * Delete User
     *
     * @param User $user
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function deleteUser(User $user): void
    {
        $user->addActivity(UserActivity::TYPE_DELETE);
        $user->setDisabled(true);
        $user->setDeleted(true);
        $this->saveUser($user);
    }
    /**
     * Delete User
     *
     * @param User $user
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function undeleteUser(User $user): void
    {
        $user->addActivity(UserActivity::TYPE_UNDELETE);
        $user->setDeleted(false);
        $this->saveUser($user);
    }

    /**
     * Delete User by id
     *
     * @param int $id
     * @return bool
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function deleteUserById(int $id): bool
    {
        if (($user = $this->find($id))) {
            $this->deleteUser($user);
            return true;
        }
        return false;
    }

    /**
     * Reset User pwToken and set pwTokenExpure to now + $interval
     * If $interval is null, set 1hour
     * Return null if not reset pwToken correctly
     *
     * @param User $user
     * @param DateInterval|null $interval
     * @return string|null
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function resetUserPwToken(User $user, DateInterval $interval = null): ?string
    {
        try {
            $interval = $interval ?: new DateInterval("P1H");
            $dateTokenExpire = new DateTime();
        } catch (Exception $e) {
            $user->setPwToken(null);
            $user->setPwTokenExpire(null);
            $this->logger->error(
                "UserFacade: resetUserPwToken - can not set PwTokenExpire. " .
                $e->getMessage()
            );
            return null;
        }
        $dateTokenExpire->add($interval);

        $maxCount = 10;
        $token = null;
        for ($i = 0; $i < $maxCount; $i++) {
            $token = md5(md5(uniqid((string)rand(), true)));
            if ($this->findUserByPwToken($token)) {
                $token = null;
            } else {
                break;
            }
        }
        if ($token) {
            $user->setPwToken($token);
            $user->setPwTokenExpire($dateTokenExpire);
            $this->saveUser($user);
            return $token;
        } else {
            $this->logger->error(
                "UserFacade: resetUserPwToken - can not set token."
            );
            return null;
        }
    }
}
