<?php
declare(strict_types=1);

namespace Mepatek\UserManager;

use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Mepatek\UserManager\Model\Role;
use Mepatek\UserManager\Model\UserManagerService;
use Nette\Security\Permission;

/**
 * Class Authorizator
 * @package Mepatek\UserManager
 */
class Authorizator extends Permission
{

    /** @var UserManagerService */
    private $userManagerService;
    /** @var Role[] */
    private $roles;

    /**
     * Authorizator constructor.
     * @param UserManagerService $userManagerService
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function __construct(
        UserManagerService $userManagerService
    ) {
        $this->userManagerService = $userManagerService;
        $this->roles = $this->userManagerService->getRoleFacade()->getCachedRoles();
        foreach ($this->roles as $role) {
            $this->addRoleRec($role);
        }

        $resources = $this->userManagerService->getResourceFacade()->getResources();
        foreach ($resources as $resource) {
            $this->addResource($resource->getResource());
        }

        // acl
        $acls = $this->userManagerService->getAclFacade()->getCachedAcls();
        foreach ($acls as $acl) {
            if ($acl->getAllowed() !== null) {
                $this->allow($acl->getRole()->getRole(), $acl->getResource(), $acl->getAllowArray());
            }
            if ($acl->getDenied() !== null) {
                $this->deny($acl->getRole()->getRole(), $acl->getResource(), $acl->getDenyArray());
            }
        }
    }

    private function addRoleRec(Role $role): void
    {
        if ($this->hasRole($role->getRole())) {
            return;
        }
        foreach ($role->getParentRolesArray() as $parentRoleStr) {
            if (!$this->hasRole($parentRoleStr)) {
                $parentRole = $this->findRoleByRole($parentRoleStr);
                $this->addRoleRec($parentRole);
            }
        }
        $this->addRole($role->getRole(), $role->getParentRolesArray());
    }

    /**
     * @param string $roleStr
     * @return Role|null
     */
    private function findRoleByRole(string $roleStr): ?Role
    {
        foreach ($this->roles as $role) {
            if ($role->getRole()==$roleStr) {
                return $role;
            }
        }
        return null;
    }
}
