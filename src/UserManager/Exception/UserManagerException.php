<?php
declare(strict_types=1);

namespace Mepatek\UserManager\Exception;

class UserManagerException extends \Exception
{
}
