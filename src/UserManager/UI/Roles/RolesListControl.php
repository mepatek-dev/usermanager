<?php
declare(strict_types=1);

namespace Mepatek\UserManager\UI\Roles;

use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Mepatek\Components\UI\GridFactory;
use Mepatek\UserManager\Model\UserManagerService;
use Mepatek\UserManager\UI\RolesFormFactory;
use Nette\Application\UI\Control;
use Ublaboo\DataGrid\Column\Action\Confirmation\StringConfirmation;
use Ublaboo\DataGrid\DataGrid;

/**
 * Class RolesListControl
 * @package Mepatek\UserManager\UI\Roles
 */
class RolesListControl extends Control
{
    /** @var RolesFormFactory */
    private $rolesFormFactory;
    /** @var UserManagerService */
    private $userManagerService;
    /** @var GridFactory */
    private $gridFactory;
    /** @var string */
    private $linkEdit;
    /** @var boolean */
    private $permittedDelete;

    /**
     * RolesListControl constructor.
     *
     * @param RolesFormFactory $rolesFormFactory
     * @param UserManagerService $userManagerService
     * @param GridFactory $gridFactory
     * @param bool $permittedDelete
     * @param string $linkEdit
     */
    public function __construct(
        RolesFormFactory $rolesFormFactory,
        UserManagerService $userManagerService,
        GridFactory $gridFactory,
        bool $permittedDelete = false,
        ?string $linkEdit = null
    ) {
        $this->rolesFormFactory = $rolesFormFactory;
        $this->userManagerService = $userManagerService;
        $this->gridFactory = $gridFactory;
        $this->permittedDelete = $permittedDelete;
        $this->linkEdit = $linkEdit;
    }

    public function render()
    {
        $template = $this->getTemplate();
        if (isset($this->parent->translator)) {
            $template->setTranslator($this->parent->translator);
        }

        $template->render(__DIR__ . '/' . substr(__CLASS__, strrpos(__CLASS__, '\\') + 1) . '.latte');
    }

    /**
     * @param string $name
     * @return DataGrid
     * @throws \Throwable
     * @throws \Ublaboo\DataGrid\Exception\DataGridException
     */
    public function createComponentRolesListGrid($name)
    {
        $roles = $this->userManagerService->getRoleFacade()->getCachedRoles();

        $grid = $this->gridFactory->create(
            $roles,
            "id",
            20,
            $this,
            $name
        );

        $grid->setColumnsHideable();

        $grid->addColumnText("role", "rolemanager.role");
        $grid->addColumnText("name", "rolemanager.role_name")
            ->setFilterText();
        $grid->addColumnText("description", "rolemanager.role_description")
            ->setDefaultHide();

        if ($this->linkEdit) {
            $grid->addAction("roleEdit", "")
                ->setTitle("rolemanager.role_action_edit")
                ->setIcon("pencil");
        }
        if ($this->permittedDelete) {
            $grid->addAction("roleDelete", "")
                ->setConfirmation(new StringConfirmation(
                        "rolemanager.role_action_delete_confirm",
                        "role"
                    )
                )
                ->setTitle("rolemanager.role_action_delete")
                ->setIcon("trash");
        }
        return $grid;
    }

    /**
     * @param $role
     * @throws \Nette\Application\AbortException
     */
    public function handleRoleEdit($id)
    {
        $this->getPresenter()->redirect($this->linkEdit, ["id" => $id]);
    }

    /**
     * @param string|int $id
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function handleRoleDelete($id)
    {
        if ($id) {
            $role = $this->userManagerService->getRoleFacade()->find((int)$id);
            $this->rolesFormFactory->deleteRole($role);
        }
    }
}
