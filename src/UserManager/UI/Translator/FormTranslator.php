<?php
declare(strict_types=1);

namespace Mepatek\UserManager\UI\Translator;

use Nette\Localization\ITranslator;
use Nette\Neon\Neon;

/**
 * Class FormTranslator
 * @package Mepatek\UserManager\UI\Translator
 */
class FormTranslator implements ITranslator
{
    /** @var string|null */
    static $dumpNotFoundNeonFile = null;
    /** @var array */
    private $dictionary = [];

    /**
     * @return FormTranslator
     */
    public static function getInstance(): FormTranslator
    {
        static $instance = null;
        if ($instance === null) {
            $instance = new FormTranslator();
        }
        return $instance;
    }

    /**
     * @return array
     */
    public function getDictionary(): array
    {
        return $this->dictionary;
    }

    /**
     * @param array $dictionary
     * @return FormTranslator
     */
    public function setDictionary(array $dictionary): FormTranslator
    {
        $this->dictionary = $dictionary;
        return $this;
    }

    /**
     * @param array $dictionary
     * @return FormTranslator
     */
    public function addDictionary(array $dictionary): FormTranslator
    {
        $this->dictionary = $this->dictionary + $dictionary;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getDumpNotFoundNeonFile(): ?string
    {
        return self::$dumpNotFoundNeonFile;
    }

    /**
     * @param string|null $dumpNotFoundNeonFile
     * @return FormTranslator
     */
    public function setDumpNotFoundNeonFile(?string $dumpNotFoundNeonFile): FormTranslator
    {
        self::$dumpNotFoundNeonFile = $dumpNotFoundNeonFile;
        return $this;
    }

    /**
     * @param $message
     * @param mixed ...$parameters
     * @return string
     */
    public function translate($message, ...$parameters): string
    {
        $message = (string)$message;
        if (key_exists($message, $this->dictionary)) {
            return $this->dictionary[$message];
        } else {
            if ($this->getDumpNotFoundNeonFile()) {
                $this->dumpNotFound($message);
            }
            return $message;
        }
    }

    /**
     * @param string $message
     */
    private function dumpNotFound(string $message): void
    {
        if (trim($message)) {
            // not found - array key translate
            if (file_exists(self::$dumpNotFoundNeonFile)) {
                $dumpExists = Neon::decode(file_get_contents(self::$dumpNotFoundNeonFile));
            } else {
                $dumpExists["translate"] = [];
            }
            if (!array_key_exists($message, $dumpExists["translate"])) {
                $dumpExists["translate"][$message] = $message;
                file_put_contents(self::$dumpNotFoundNeonFile, Neon::encode($dumpExists, Neon::BLOCK));
            }
        }
    }
}
