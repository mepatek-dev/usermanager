<?php
declare(strict_types=1);

namespace Mepatek\UserManager\UI\Translator;

use Nette\Caching\Cache;
use Nette\Caching\IStorage;
use Nette\Caching\Storages\FileStorage;
use Nette\Neon\Neon;
use Nette\Utils\Finder;

/**
 * Class FormTranslations
 * @package Mepatek\UserManager\UI\Translator
 */
class FormTranslations
{
    /** @var Cache */
    private $cache;
    /** @var string|null */
    private $languageFolder = null;

    /**
     * FormTranslations constructor.
     * @param IStorage $storage
     */
    public function __construct(
        IStorage $storage
    ) {
        $this->cache = new Cache($storage, "Usermanager.FormTranslations");
    }

    /**
     * @param string $language
     * @param string $defaultLanguage
     * @return array
     */
    public function getDictionaries(
        string $language,
        string $defaultLanguage = "cs",
        string $folder = null
    ): array {
        $folder = $folder ?: $this->getLanguageFolder();
        $key = md5($folder) . "_" . $language;
        $dictionary = $this->cache->load($key, function (&$dependencies) use ($language, $defaultLanguage, $folder) {
            $files = [];
            $dictionary = [];
            $finder = Finder::findFiles("*." . $language . ".neon")
                ->in($folder);
            foreach ($finder as $file => $spl) {
                $translate = Neon::decode(file_get_contents($file));
                $dictionary = $dictionary + $translate["translate"];
                $files[] = $file;
            }
            $dependencies = [
                Cache::FILES => $files,
            ];
            return count($dictionary) > 0 ? $dictionary : null;
        });
        return $dictionary;
    }

    /**
     * @return string|null
     */
    public function getLanguageFolder(): ?string
    {
        $languageFolder = trim(
            $this->languageFolder
                ?:
                (__DIR__ . DIRECTORY_SEPARATOR . "Languages")
        );
        $languageFolder = (in_array(substr($languageFolder, -1), ["/", "\\"])) ?
            $languageFolder : ($languageFolder . DIRECTORY_SEPARATOR);
        return $languageFolder;
    }

    /**
     * @param string|null $languageFolder
     * @return FormTranslations
     */
    public function setLanguageFolder(?string $languageFolder): FormTranslations
    {
        $this->languageFolder = $languageFolder;
        return $this;
    }
}
