<?php
declare(strict_types=1);

namespace Mepatek\UserManager\UI;

use Mepatek\Components\International\LanguageHelper;
use Mepatek\Components\UI\FormFactory;
use Mepatek\Components\UI\GridFactory;
use Mepatek\UserManager\Authorizator;
use Mepatek\UserManager\Model\Acl;
use Mepatek\UserManager\Model\Role;
use Mepatek\UserManager\Model\UserManagerService;
use Mepatek\UserManager\UI\Roles\RolesListControl;
use Mepatek\UserManager\UI\Translator\FormTranslations;
use Mepatek\UserManager\UI\Translator\FormTranslator;
use Nette\Application\UI\Form;
use Nette\Caching\IStorage;
use Nette\Localization\ITranslator;
use Nette\SmartObject;

class RolesFormFactory
{
    use SmartObject;

    /** @var array */
    public $onBeforeRoleSave = [];
    /** @var array */
    public $onAfterRoleSave = [];
    /** @var array */
    public $onBeforeRoleDelete = [];
    /** @var array */
    public $onAfterRoleDelete = [];
    /**
     * Event - call everytime - for redraws (ajax)
     * @var array
     */
    public $onRedraw = [];
    /** @var UserManagerService */
    private $userManagerService;
    /** @var GridFactory */
    private $gridFactory;
    /** @var FormFactory */
    private $formFactory;
    /** @var LanguageHelper */
    private $languageHelper;
    /** @var Authorizator */
    private $authorizator;
    /**
     * Translator. If set all texts are translated
     * @var ITranslator
     */
    private $translator = null;
    /** @var IStorage */
    private $storage;

    /**
     * RoleManagerFormFactory constructor.
     *
     * @param UserManagerService $userManagerService
     * @param GridFactory $gridFactory
     * @param FormFactory $formFactory
     */
    public function __construct(
        UserManagerService $userManagerService,
        GridFactory $gridFactory,
        FormFactory $formFactory,
        LanguageHelper $languageHelper,
        Authorizator $authorizator,
        IStorage $storage
    ) {
        $this->userManagerService = $userManagerService;
        $this->gridFactory = $gridFactory;
        $this->formFactory = $formFactory;
        $this->languageHelper = $languageHelper;
        $this->authorizator = $authorizator;
        $this->storage = $storage;
    }

    /**
     * @param bool $permittedDelete
     * @param string $linkRoleEdit
     * @return RolesListControl
     */
    public function createRolesList(
        bool $permittedDelete = false,
        ?string $linkRoleEdit = null
    ): RolesListControl {
        $rolesListControl = new RolesListControl(
            $this,
            $this->userManagerService,
            $this->gridFactory,
            $permittedDelete,
            $linkRoleEdit
        );
        return $rolesListControl;
    }

    /**
     * @param Role $role
     * @return Form
     */
    public function createRoleEditForm(Role $role): Form
    {
        $form = $this->getForm();

        $form->addSubmit("send", "rolemanager.role_save");

        $form->addGroup("Role");

        $form->addText("role", "rolemanager.role")
            ->setDisabled(true);
        $form->addText("name", "rolemanager.role_name")
            ->setRequired("rolemanager.role_required");
        $form->addTextArea("description", "rolemanager.role_description");
        $form->addCheckboxList(
            "parentRoles",
            "Dědí oprávnění od",
            $this->getRolesList()
        );

        $resources = $this->userManagerService->getResourceFacade()->getResources();
        $form->setDefaults(
            [
                "role" => $role->getRole(),
                "name" => $role->getName(),
                "description" => $role->getDescription(),
                "parentRoles"=>$role->getParentRolesId(),
            ]
        );

        foreach ($resources as $resource) {
            $form->addGroup("Oprávnění - " . $resource->getTitle());
            foreach ($resource->getPrivileges() as $privilege => $privilegeTitle) {
                $inputName = $resource->getResource() . "_" . $privilege;
                $isAllowed = $this->authorizator->isAllowed(
                    $role->getRole(),
                    $resource->getResource(),
                    $privilege
                );
                $form->addCheckbox($inputName, $privilegeTitle)
                    ->setDefaultValue($isAllowed);
            }
        }

        $form->onSuccess[] = function (Form $form, $values) use ($role, $resources) {

            $this->onBeforeRoleSave($role);
            foreach ($resources as $resource) {
                foreach ($resource->getPrivileges() as $privilege => $privilegeTitle) {
                    $inputName = $resource->getResource() . "_" . $privilege;
                    $acl = $this->userManagerService->getAclFacade()
                        ->findByRoleAndResource($role, $resource->getResource());
                    if (!$acl) {
                        $acl = new Acl();
                        $acl->setRole($role);
                        $acl->setResource($resource->getResource());
                    }
                    $isAllowed = $values->$inputName;
                    if ($isAllowed) {
                        $acl->allow($privilege);
                    } else {
                        $acl->deny($privilege);
                    }
                    $this->userManagerService->getAclFacade()
                        ->saveAcl($acl);
                }
            }
            $parentRoles = [];
            foreach ($values->parentRoles as $parentRoleId) {
                $parentRole = $this->userManagerService->getRoleFacade()->find((int)$parentRoleId);
                $parentRoles[] = $parentRole;
            }
            $role->setParentRoles($parentRoles);
            $role->setName($values->name);
            $role->setDescription($values->description);
            $this->userManagerService->getRoleFacade()->saveRole($role);
            $this->onAfterRoleSave($role);
        };

        return $form;
    }

    /**
     * @return Form
     */
    public function createRoleNewForm(): Form
    {
        $form = $this->getForm();

        $form->addSubmit("send", "rolemanager.role_save");

        $form->addGroup("Nová role");

        $form->addText("role", "rolemanager.role")
            ->setRequired("rolemanager.role_required");
        $form->addText("name", "rolemanager.role_name")
            ->setRequired("rolemanager.role_required");
        $form->addTextArea("description", "rolemanager.role_description");

        $form->addCheckboxList(
            "parentRoles",
            "Dědí oprávnění od",
            $this->getRolesList()
        );

        $resources = $this->userManagerService->getResourceFacade()->getResources();

        foreach ($resources as $resource) {
            $form->addGroup("Oprávnění - " . $resource->getTitle());
            foreach ($resource->getPrivileges() as $privilege => $privilegeTitle) {
                $inputName = $resource->getResource() . "_" . $privilege;
                $form->addCheckbox($inputName, $privilegeTitle)
                    ->setDefaultValue(false);
            }
        }

        $form->onSuccess[] = function (Form $form, $values) use ($resources) {

            $role = new Role();
            $this->onBeforeRoleSave($role);

            $parentRoles = [];
            foreach ($values->parentRoles as $parentRoleId) {
                $parentRole = $this->userManagerService->getRoleFacade()->find((int)$parentRoleId);
                $parentRoles[] = $parentRole;
            }
            $role->setParentRoles($parentRoles);

            $role->setRole($values->role);
            $role->setName($values->name);
            $role->setDescription($values->description);
            $this->userManagerService->getRoleFacade()->saveRole($role);

            foreach ($resources as $resource) {
                foreach ($resource->getPrivileges() as $privilege => $privilegeTitle) {
                    $inputName = $resource->getResource() . "_" . $privilege;
                    $acl = $this->userManagerService->getAclFacade()
                        ->findByRoleAndResource($role, $resource->getResource());
                    if (!$acl) {
                        $acl = new Acl();
                        $acl->setRole($role);
                        $acl->setResource($resource->getResource());
                    }
                    $isAllowed = $values->$inputName;
                    if ($isAllowed) {
                        $acl->allow($privilege);
                    } else {
                        $acl->deny($privilege);
                    }
                    $this->userManagerService->getAclFacade()
                        ->saveAcl($acl);
                }
            }
            $this->onAfterRoleSave($role);
        };

        return $form;
    }

    /**
     * @param Role $role
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function deleteRole(Role $role): void
    {
        $this->onBeforeRoleDelete($role);
        $this->userManagerService->getRoleFacade()->permanentDeleteRole($role);
        $this->onAfterRoleDelete($role);
    }

    /**
     * Get Form object
     * Set translator if is set
     * @return Form
     */
    public function getForm(): Form
    {
        $form = $this->formFactory->create();
        // set translator
        if (!$form->getTranslator()) {
            $form->setTranslator($this->getTranslator());
        }
        $form->onSubmit[] = function () {
            $this->onRedraw();
        };
        return $form;
    }

    /**
     * @return ITranslator
     */
    public function getTranslator(): ITranslator
    {
        if (!$this->translator) {
            $this->translator = FormTranslator::getInstance();
            $this->translator->addDictionary(
                (new FormTranslations($this->storage))->getDictionaries($this->languageHelper->detectLanguage())
            );
        }
        return $this->translator;
    }

    /**
     * @param ITranslator $translator
     * @return RolesFormFactory
     */
    public function setTranslator(ITranslator $translator): RolesFormFactory
    {
        $this->translator = $translator;
        return $this;
    }

    /**
     * @param Role|null $withoutRole
     * @return array
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    private function getRolesList(?Role $withoutRole = null): array
    {
        $roles = [];
        foreach ($this->userManagerService->getRoleFacade()->getCachedRoles() as $role) {
            if (!$withoutRole or $role->getId() <> $withoutRole->getId()) {
                $roles[$role->getId()] = $role->getRole() . " (" . $role->getName() . ")";
            }
        }
        return $roles;
    }
}
