<?php
declare(strict_types=1);

namespace Mepatek\UserManager\UI;

use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Mepatek\Components\International\LanguageHelper;
use Mepatek\Components\UI\FormFactory;
use Mepatek\Components\UI\GridFactory;
use Mepatek\UserManager\Authenticator;
use Mepatek\UserManager\Model\User;
use Mepatek\UserManager\Model\UserManagerService;
use Mepatek\UserManager\UI\Translator\FormTranslations;
use Mepatek\UserManager\UI\Translator\FormTranslator;
use Mepatek\UserManager\UI\Users\UsersListControl;
use Nette\Application\UI\Form;
use Nette\Caching\IStorage;
use Nette\Http\FileUpload;
use Nette\Localization\ITranslator;
use Nette\SmartObject;
use Ublaboo\DataGrid\DataGrid;
use Ublaboo\DataGrid\Exception\DataGridException;

class UsersFormsFactory
{
    use SmartObject;

    /** @var array */
    public $onBeforeUserSave = [];
    /** @var array */
    public $onAfterUserSave = [];
    /** @var array */
    public $onBeforeUserDelete = [];
    /** @var array */
    public $onAfterUserDelete = [];
    /** @var array */
    public $onBeforeUserChangePassword = [];
    /** @var array */
    public $onAfterUserChangePassword = [];
    /**
     * Event - call everytime - for redraws (ajax)
     * @var array
     */
    public $onRedraw = [];
    /** @var UserManagerService */
    private $userManagerService;
    /** @var Authenticator */
    private $authenticator;
    /** @var GridFactory */
    private $gridFactory;
    /** @var FormFactory */
    private $formFactory;
    /** @var LanguageHelper */
    private $languageHelper;
    /** @var IStorage */
    private $storage;
    /**
     * Translator. If set all texts are translated
     * @var ITranslator
     */
    private $translator = null;

    /**
     * UserManagerFormsFactory constructor.
     *
     * @param UserManagerService $userManagerService
     * @param GridFactory $gridFactory
     * @param FormFactory $formFactory
     * @param LanguageHelper $languageHelper
     * @param Authenticator $authenticator
     */
    public function __construct(
        UserManagerService $userManagerService,
        GridFactory $gridFactory,
        FormFactory $formFactory,
        LanguageHelper $languageHelper,
        Authenticator $authenticator,
        IStorage $storage
    ) {
        $this->userManagerService = $userManagerService;
        $this->gridFactory = $gridFactory;
        $this->formFactory = $formFactory;
        $this->languageHelper = $languageHelper;
        $this->authenticator = $authenticator;
        $this->storage = $storage;
    }

    /**
     * @param bool $permittedDelete
     * @param string|null $linkUserEdit
     * @param string|null $linkUserChangePassword
     * @return UsersListControl
     */
    public function createUsersList(
        bool $permittedDelete = false,
        ?string $linkUserEdit = null,
        ?string $linkUserChangePassword = null
    ): UsersListControl {
        $usersListControl = new UsersListControl(
            $this,
            $this->userManagerService,
            $this->gridFactory,
            $permittedDelete,
            $linkUserEdit,
            $linkUserChangePassword
        );
        return $usersListControl;
    }

    /**
     * @param User $user
     * @return Form
     */
    public function createUserEditForm(?User $user): Form
    {
        $form = $this->getForm();

        $form->addText("userName", "usermanager.user_name")
            ->setRequired("usermanager.user_name_required");
        $form->addText("fullName", "usermanager.user_full_name")
            ->setRequired("userform.user_full_name_required");
        $form->addText("title", "usermanager.user_title");
        $form->addText("email", "usermanager.user_email")
            ->setRequired("userform.user_email_required")
            ->addRule(Form::EMAIL, "userform.user_email_not_correct");
        $form->addText("phone", "usermanager.user_phone");
        $form->addSelect("language", "usermanager.user_language")
            ->setPrompt("usermanager.user_select_language")
            ->setItems(
                $this->languageHelper->getSelectItems(
                    $user ? $user->getLanguage() : null
                )
            );
        $form->addSelect("disabled", "usermanager.user_disabled_caption")
            ->setItems(
                [
                    0 => "usermanager.user_enabled",
                    1 => "usermanager.user_disabled",
                ]
            );
        $form->addUpload("thumbnail", "usermanager.user_thumbnail");
        // usermanager.user_created
        // usermanager.user_last_logged

        $form->addSubmit("send", "usermanager.user_save");
        if ($user) {
            $form->setDefaults(
                [
                    "userName" => $user->getUserName(),
                    "fullName" => $user->getFullName(),
                    "title" => $user->getTitle(),
                    "email" => $user->getEmail(),
                    "phone" => $user->getPhone(),
                    "language" => $user->getLanguage(),
                    "disabled" => $user->isDisabled() ? 1 : 0,
                ]
            );
        }

        $form->onSuccess[] = function (Form $form, $values) use ($user) {
            $userFacade = $this->userManagerService->getUserFacade();

            if (!$user) {
                $user = new User();
            }

            $user->setUserName($values->userName);
            $user->setFullName($values->fullName);
            $user->setTitle($values->title);
            $user->setEmail($values->email);
            $user->setPhone($values->phone);
            $user->setLanguage($values->language);
            $user->setDisabled((boolean)$values->disabled);

            /** @var FileUpload $thumbnail */
            $thumbnail = $values->thumbnail;
            if ($thumbnail->isOk() and $thumbnail->isImage()) {
                $user->setThumbnail($thumbnail->getContents());
            }
            $this->onBeforeUserSave($user);
            $userFacade->saveUser($user);
            $this->onAfterUserSave($user);
        };

        return $form;
    }

    /**
     * @return Form
     */
    public function createUserNewForm(): Form
    {
        $form = $this->getForm();

        $form->addText("userName", "usermanager.user_name")
            ->setRequired("usermanager.user_name_required");
        $form->addText("fullName", "usermanager.user_full_name")
            ->setRequired("userform.user_full_name_required");
        $form->addText("title", "usermanager.user_title");
        $form->addText("email", "usermanager.user_email")
            ->setRequired("userform.user_email_required")
            ->addRule(Form::EMAIL, "userform.user_email_not_correct");
        $form->addText("phone", "usermanager.user_phone");
        $form->addSelect("language", "usermanager.user_language")
            ->setPrompt("usermanager.user_select_language")
            ->setItems(
                $this->languageHelper->getSelectItems(null)
            );
        $form->addSelect("disabled", "usermanager.user_disabled_caption")
            ->setItems(
                [
                    0 => "usermanager.user_enabled",
                    1 => "usermanager.user_disabled",
                ]
            );
        $form->addUpload("thumbnail", "usermanager.user_thumbnail");
        // usermanager.user_created
        // usermanager.user_last_logged

        $form->addSubmit("send", "usermanager.user_save");

        $form->onSuccess[] = function (Form $form, $values) {
            $userFacade = $this->userManagerService->getUserFacade();

            $user = new User();
            $user->setUserName($values->userName);
            $user->setFullName($values->fullName);
            $user->setTitle($values->title);
            $user->setEmail($values->email);
            $user->setPhone($values->phone);
            $user->setLanguage($values->language);
            $user->setDisabled((boolean)$values->disabled);

            /** @var FileUpload $thumbnail */
            $thumbnail = $values->thumbnail;
            if ($thumbnail->isOk() and $thumbnail->isImage()) {
                $user->setThumbnail($thumbnail->getContents());
            }
            $this->onBeforeUserSave($user);
            $userFacade->saveUser($user);
            $this->onAfterUserSave($user);
        };

        return $form;
    }

    /**
     * @param User $user
     * @return Form
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function createComponentUserRoleForm(User $user)
    {

        $form = $this->getForm();
        $roles = $this->userManagerService->getRoleFacade()
            ->getRoles();

        foreach ($roles as $role) {
            $checkbox = (string)$role->getId();
            $form->addCheckbox($checkbox, $role->getName() ?: $role->getRole())
                ->setDefaultValue($user->isInRole($role->getRole()));
        }

        $form->addSubmit("send", "Ulož role");

        $form->onSuccess[] = function ($form, $values) use ($user) {
            $user->resetRoles();
            foreach ($values as $key => $value) {
                if ($value and $key !== "id") {
                    $role = $this->userManagerService->getRoleFacade()->find((int)$key);
                    if ($role) {
                        $user->addRole($role);
                    }
                }
            }
            $this->userManagerService->getUserFacade()->saveUser($user);
        };
        return $form;
    }

    /**
     * Create password change form component
     *
     * id(hidden)
     * newPassword(password)
     * newPasswordConfirm(password)
     * send(submit)
     *
     * @return Form
     */
    public function createChangePasswordForm(User $user): Form
    {
        $form = $this->getForm();

        $form->addPassword('newPassword', "usermanager.user_new_password")
            ->addRule(
                Form::MIN_LENGTH,
                "usermanager.user_new_password_min_length",
                $this->authenticator->passwordMinLength
            )
            ->setRequired("usermanager.user_new_password_required");
        $form->addPassword('newPasswordConfirm', "usermanager.user_new_password_confirm")
            ->setRequired("usermanager.user_new_password_confirm_required")
            ->addRule(Form::EQUAL, "usermanager.user_new_password_not_same", $form['newPassword']);
        $form->addSubmit('send', "usermanager.user_change_password");

        $form->onSuccess[] = function (Form $form, $values) use ($user) {

            $this->onBeforeUserChangePassword($user, $values->newPassword);

            if (!$this->checkPassword($form, $values->newPassword)) {
                return false;
            }

            if (!$this->authenticator->changePassword($user->getId(), $values->newPassword)) {
                $form->addError("usermanager.user_new_password_not_change");
                return false;
            }

            $this->onAfterUserChangePassword($user, $values->newPassword);
            return true;
        };
        return $form;
    }

    /**
     * @param User $user
     * @return DataGrid
     * @throws DataGridException
     */
    public function createUserActivityGrid(User $user): DataGrid
    {
        $grid = $this->gridFactory->create(
            $user->getActivities(),
            "id",
            20
        );
        if (!$grid->getTranslator()) {
            $grid->setTranslator($this->getTranslator());
        }
        $grid->addColumnText("type", "userform.useractivity_type")
            ->setSortable()
            ->setFilterText();
        $grid->addColumnDateTime("datetime", "userform.useractivity_datetime")
            ->setFormat("j. n. Y H:i:s")
            ->setAlign("left")
            ->setSortable();
        $grid->addColumnText("ip", "userform.useractivity_ip")
            ->setSortable()
            ->setFilterText();
        $grid->addColumnText("description", "userform.useractivity_description")
            ->setFilterText();
        $grid->setDefaultSort(["datetime" => "DESC"]);
        return $grid;
    }

    /**
     * Get Form object
     * Set translator if is set
     * @return Form
     */
    public function getForm(): Form
    {
        $form = $this->formFactory->create();
        // set translator
        if (!$form->getTranslator()) {
            $form->setTranslator($this->getTranslator());
        }
        $form->onSubmit[] = function () {
            $this->onRedraw();
        };
        return $form;
    }

    /**
     * @param User $user
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function deleteUser(User $user): void
    {
        $this->onBeforeUserDelete($user);
        $this->userManagerService->getUserFacade()->deleteUser($user);
        $this->onAfterUserDelete($user);
    }

    /**
     * @param User $user
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function undeleteUser(User $user): void
    {
        $this->onBeforeUserSave($user);
        $this->userManagerService->getUserFacade()->undeleteUser($user);
        $this->onAfterUserSave($user);
    }

    /**
     * @return UserManagerService
     */
    public function getUserManagerService(): UserManagerService
    {
        return $this->userManagerService;
    }

    /**
     * @param UserManagerService $userManagerService
     * @return UsersFormsFactory
     */
    public function setUserManagerService(UserManagerService $userManagerService): UsersFormsFactory
    {
        $this->userManagerService = $userManagerService;
        return $this;
    }

    /**
     * @return ITranslator
     */
    public function getTranslator(): ITranslator
    {
        if (!$this->translator) {
            $this->translator = FormTranslator::getInstance();
            $this->translator->addDictionary(
                (new FormTranslations($this->storage))->getDictionaries($this->languageHelper->detectLanguage())
            );
        }
        return $this->translator;
    }

    /**
     * @param ITranslator $translator
     * @return UsersFormsFactory
     */
    public function setTranslator(ITranslator $translator): UsersFormsFactory
    {
        $this->translator = $translator;
        return $this;
    }

    /**
     * @param Form $form
     * @param string $newPassword
     * @return bool
     */
    protected function checkPassword(Form $form, string $newPassword): bool
    {
        if (($passwordSafe = $this->authenticator
                ->isPasswordSafe($newPassword)) > 0
        ) {
            if ($passwordSafe == 2 or $passwordSafe == 6) {
                $form->addError("usermanager.user_new_password_min_length_1");
            }
            if ($passwordSafe == 4 or $passwordSafe == 6) {
                $form->addError("usermanager.user_new_password_too_simple");
            }
            return false;
        } else {
            return true;
        }
    }
}
