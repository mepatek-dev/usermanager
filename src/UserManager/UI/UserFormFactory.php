<?php
declare(strict_types=1);

namespace Mepatek\UserManager\UI;

use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Mepatek\Components\International\LanguageHelper;
use Mepatek\Components\UI\FormFactory;
use Mepatek\Components\UI\GridFactory;
use Mepatek\UserManager\Authenticator;
use Mepatek\UserManager\Exception\UserManagerException as UserManagerExceptionAlias;
use Mepatek\UserManager\Model\UserManagerService;
use Mepatek\UserManager\UI\Translator\FormTranslations;
use Mepatek\UserManager\UI\Translator\FormTranslator;
use Nette\Application\UI\Form;
use Nette\Caching\IStorage;
use Nette\Http\FileUpload;
use Nette\Localization\ITranslator;
use Nette\Security\AuthenticationException;
use Nette\Security\User;
use Nette\SmartObject;
use Nette\Utils\Validators;
use Ublaboo\DataGrid\DataGrid;
use Ublaboo\DataGrid\Exception\DataGridException;

/**
 * Class UserFormFactory
 * @package Mepatek\UserManager\UI
 */
class UserFormFactory
{
    use SmartObject;
    /**
     * Expiration time for not remember user
     * @var string
     */
    public $expirationTime = "60 minutes";
    /**
     * Expiration time for remember user
     * @var string
     */
    public $expirationTimeRemember = "7 days";
    /**
     * Translator. If set all texts are translated
     * @var ITranslator
     */
    public $translator = null;
    /**
     * Event - call everytime - for redraws (ajax)
     * @var array
     */
    public $onRedraw = [];
    /**
     * Event - login success
     * @var array
     */
    public $onLoginSuccess = [];
    /**
     * Event - forgot success ($email, $token)
     * @var array
     */
    public $onForgotSuccess = [];
    /**
     * If not set token (ie not exist email) do not generate error (security!)
     * @var bool
     */
    public $ignoreForgotError = true;
    /**
     * Event - recovery success
     * @var array
     */
    public $onRecoverySuccess = [];
    /**
     * Event - recovery not change password
     * @var array
     */
    public $onRecoveryNotChangePassword = [];
    /**
     * Event - change password success
     * @var array
     */
    public $onChangePasswordSuccess = [];
    /**
     * Event - not change password
     * @var array
     */
    public $onNotChangePassword = [];
    /** @var array */
    public $onBeforeUserSave = [];
    /** @var array */
    public $onAfterUserSave = [];
    /** @var User (must set) */
    private $user;
    /** @var UserManagerService */
    private $userManagerService;
    /** @var Authenticator */
    private $authenticator;
    /** @var GridFactory */
    private $gridFactory;
    /** @var FormFactory */
    private $formFactory;
    /** @var LanguageHelper */
    private $languageHelper;
    /** @var IStorage */
    private $storage;

    /**
     * UserManagerFormsFactory constructor.
     *
     * @param User $user
     * @param GridFactory $gridFactory
     * @param FormFactory $formFactory
     * @param LanguageHelper $languageHelper
     * @param UserManagerService $userManagerService
     * @param Authenticator $authenticator
     */
    public function __construct(
        User $user,
        GridFactory $gridFactory,
        FormFactory $formFactory,
        LanguageHelper $languageHelper,
        UserManagerService $userManagerService,
        Authenticator $authenticator,
        IStorage $storage
    ) {
        $this->user = $user;
        $this->gridFactory = $gridFactory;
        $this->formFactory = $formFactory;
        $this->languageHelper = $languageHelper;
        $this->userManagerService = $userManagerService;
        $this->authenticator = $authenticator;
        $this->storage = $storage;
    }

    /**
     * Create form component for login
     *
     * username(text)
     * password(password)
     * remember(checkbox)
     * send(submit)
     *
     * @return Form
     */
    public function createLoginForm(): Form
    {
        $form = $this->getForm();
        $form->addText('username', 'usermanager.user_name')
            ->setRequired("usermanager.user_name_required");

        $form->addPassword('password', "usermanager.user_password")
            ->setRequired("usermanager.user_password_required");

        $form->addCheckbox('remember', "usermanager.remember");

        $form->addSubmit('send', "usermanager.user_sign_in");

        $form->onValidate[] = [$this, 'loginFormValidate'];

        $form->onSuccess[] = [$this, 'loginFormSucceeded'];
        return $form;
    }

    /**
     * @param Form $form
     * @return bool
     */
    public function loginFormValidate(Form $form): bool
    {
        $values = $form->getValues();
        if (!$values->username) {
            $form->addError("usermanager.user_name_required");
            return false;
        }
        if (!$values->password) {
            $form->addError("usermanager.user_password_required");
            return false;
        }
        return true;
    }

    /**
     * onSuccess event loginForm
     *
     * calls events onLoginSuccess[]
     *
     * @param Form $form
     * @param      $values
     *
     * @return bool
     */
    public function loginFormSucceeded(Form $form, $values): bool
    {
        if ($values->remember) {
            $this->user->setExpiration($this->expirationTimeRemember);
        } else {
            $this->user->setExpiration($this->expirationTime);
        }

        try {
            $this->user->login($values->username, $values->password);
        } catch (AuthenticationException $e) {
            $form->addError("usermanager.username_or_password_incorrect");
            return false;
        }

        $this->onLoginSuccess($values);

        return true;
    }

    /**
     * Create forgot password form component
     *
     * email(text)
     * send(submit)
     *
     * @return Form
     */
    public function createForgotPasswordForm(): Form
    {
        $form = $this->getForm();
        $form->addText('email', "usermanager.forgot_email")
            ->setRequired("usermanager.forgot_email_required");

        $form->addSubmit('send', "usermanager.forgot_password");

        $form->onSuccess[] = [$this, 'forgotPasswordFormSucceeded'];
        return $form;
    }

    /**
     * onSuccess event loginForm
     *
     * calls events onForgotSuccess[]
     *
     * @param Form $form
     * @param      $values
     *
     * @return bool
     */
    public function forgotPasswordFormSucceeded(Form $form, $values): bool
    {
        // not set email correct
        if (!Validators::isEmail($values->email)) {
            $form->addError("usermanager.forgot_email_not_correct");
            return false;
        }

        // if not token set, user with email not exist
        $token = $this->user->getAuthenticator()->resetPasswordToken($values->email);
        if (!$token and !$this->ignoreForgotError) {
            $form->addError("usermanager.forgot_token_does_not_exist");
            return false;
        } else {
            $token = "";
        }

        $this->onForgotSuccess($values->email, $token);

        return true;
    }

    /**
     * Create recovery password form component
     *
     * token(hidden)
     * password(password)
     * passwordVerify(password)
     * send(submit)
     *
     * @param string $token
     *
     * @return Form
     */
    public function createRecoveryPasswordForm($token): Form
    {
        $form = $this->getForm();
        $form->addHidden("token", $token);

        $form->addPassword('newPassword', "usermanager.user_new_password")
            ->addRule(
                Form::MIN_LENGTH,
                "usermanager.user_new_password_min_length",
                $this->authenticator->passwordMinLength
            )
            ->setRequired("usermanager.user_new_password_required");
        $form->addPassword('newPasswordConfirm', "usermanager.user_new_password_confirm")
            ->setRequired("usermanager.user_new_password_confirm_required")
            ->addRule(Form::EQUAL, "usermanager.user_new_password_not_same", $form['newPassword']);
        $form->addSubmit('send', "usermanager.user_change_password");

        $form->onSuccess[] = [$this, 'recoveryPasswordFormSucceeded'];
        return $form;
    }

    /**
     * onSuccess event recoveryPasswordForm
     *
     * calls events onChangePasswordSuccess[]+onRecoverySuccess[] and onRecoveryNotChangePassword[]
     *
     * @param Form $form
     * @param      $values
     *
     * @return bool
     * @throws UserManagerExceptionAlias
     */
    public function recoveryPasswordFormSucceeded(Form $form, $values): bool
    {
        if (!$this->checkPassword($form, $values->newPassword)) {
            $this->onRecoveryNotChangePassword();
            return false;
        }

        if (!($user = $this->authenticator
            ->changePasswordToken($values->token, $values->newPassword))
        ) {
            $this->onRecoveryNotChangePassword();
            $form->addError("usermanager.password_can_not_change");
            return false;
        }

        $this->onChangePasswordSuccess($user, $values->newPassword);
        $this->onRecoverySuccess();
        return true;
    }

    /**
     * Create password change form component
     *
     * id(hidden)
     * password(password)
     * passwordVerify(password)
     * send(submit)
     *
     * @return Form
     */
    public function createChangePasswordForm(): Form
    {
        $form = $this->getForm();

        $form->addPassword('password', "usermanager.user_new_password_current")
            ->setRequired("usermanager.user_password_required");
        $form->addPassword('newPassword', "usermanager.user_new_password")
            ->addRule(
                Form::MIN_LENGTH,
                "usermanager.user_new_password_min_length",
                $this->authenticator->passwordMinLength
            )
            ->setRequired("usermanager.user_new_password_required");
        $form->addPassword('newPasswordConfirm', "usermanager.user_new_password_confirm")
            ->setRequired("usermanager.user_new_password_confirm_required")
            ->addRule(Form::EQUAL, "usermanager.user_new_password_not_same", $form['newPassword']);
        $form->addSubmit('send', "usermanager.user_change_password");

        $form->onSuccess[] = [$this, 'changePasswordFormSucceeded'];
        return $form;
    }

    /**
     * onSuccess event changePasswordForm
     *
     * calls events onChangePasswordSuccess[] and onNotChangePassword[]
     *
     * @param Form $form
     * @param      $values
     *
     * @return bool
     */
    public function changePasswordFormSucceeded(Form $form, $values): bool
    {
        $currentPassword = $values->password;
        try {
            $this->user->getAuthenticator()->authenticate([
                $this->user->getIdentity()->userName,
                $currentPassword,
            ]);
        } catch (AuthenticationException $exception) {
            $form->addError("usermanager.user_new_password_not_authenticate");
            return false;
        }

        if (!$this->checkPassword($form, $values->newPassword)) {
            $this->onNotChangePassword();
            return false;
        }

        if (!$this->user->getAuthenticator()->changePassword($this->user->getId(), $values->newPassword)) {
            $this->onNotChangePassword();
            $form->addError("usermanager.password_can_not_change");
            return false;
        }

        $this->onChangePasswordSuccess($this->user->getId(), $values->newPassword);
        return true;
    }

    /**
     * @return Form
     */
    public function createUserProfileForm(): Form
    {
        $userId = (int)$this->getUser()->getId();
        $user = $this->userManagerService->getUserFacade()->find($userId);
        $form = $this->getForm();
        $form->addHidden("id", (string)$user->getId());

        $form->addUpload("thumbnail", "usermanager.user_thumbnail");

        $form->addText("userName", "userform.user_name")
            ->setDisabled(true);
        $form->addText("fullName", "userform.user_full_name")
            ->setRequired("userform.user_full_name_required");

        $form->addText("title", "userform.user_title");
        $form->addText("email", "userform.user_email")
            ->setRequired("userform.user_email_required")
            ->addRule(Form::EMAIL, "userform.user_email_not_correct");
        $form->addText("phone", "userform.user_phone");
        $form->addSelect("language", "userform.user_language")
            ->setPrompt("userform.user_select_language")
            ->setItems(
                $this->getLanguageHelper()->getSelectItems(
                    $user->getLanguage()
                )
            );

        $form->addSubmit("send", "userform.user_save");

        $form->setDefaults(
            [
                "userName" => $user->getUserName(),
                "fullName" => $user->getFullName(),
                "title" => $user->getTitle(),
                "email" => $user->getEmail(),
                "phone" => $user->getPhone(),
                "language" => $user->getLanguage(),
            ]
        );

        $form->onSuccess[] = [$this, 'userProfileFormSucceeded'];

        return $form;
    }

    /**
     * Remove thumbnail from user
     *
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function removeThumbnail(): void
    {
        $user = $this->userManagerService->getUserFacade()->find((int)$this->getUser()->getId());
        $this->getUser()->getIdentity()->thumbnail = null;
        $user->setThumbnail(null);
        $this->onBeforeUserSave($user);
        $this->userManagerService->getUserFacade()->saveUser($user);
        $this->onAfterUserSave($user);
    }

    /**
     * onSuccess event userProfileForm
     *
     * calls events onBeforeUserSave[] and onAfterUserSave[]
     *
     * @param Form $form
     * @param iterable $values
     *
     * @return bool
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function userProfileFormSucceeded(Form $form, $values): bool
    {
        $user = $this->userManagerService->getUserFacade()->find((int)$values->id);
        $user->setFullName($values->fullName);
        $user->setTitle($values->title);
        $user->setEmail($values->email);
        $user->setPhone($values->phone);
        $user->setLanguage($values->language);
        if ($values->thumbnail->isImage()) {
            /** @var FileUpload $thumbnail */
            $thumbnail = $values->thumbnail;
            $user->setThumbnail($thumbnail->getContents());
        }

        $idenity = $this->user->getIdentity();
        if ($values->thumbnail->isImage()) {
            $idenity->thumbnail = $user->getThumbnail();
        }
        $idenity->fullName = $values->fullName;
        $idenity->title = $values->title;
        $idenity->email = $values->email;
        $idenity->phone = $values->phone;
        $idenity->language = $values->language;

        $this->onBeforeUserSave($user);
        $this->userManagerService->getUserFacade()->saveUser($user);
        $this->onAfterUserSave($user);

        return true;
    }

    /**
     * @return DataGrid
     * @throws DataGridException
     */
    public function createUserActivityGrid(): DataGrid
    {
        $userId = (int)$this->getUser()->getId();
        $user = $this->userManagerService->getUserFacade()->find($userId);

        $grid = $this->gridFactory->create(
            $user->getActivities(),
            "id",
            20
        );
        if (!$grid->getTranslator()) {
            $grid->setTranslator($this->getTranslator());
        }
        $grid->addColumnText("type", "userform.useractivity_type")
            ->setSortable()
            ->setFilterText();
        $grid->addColumnDateTime("datetime", "userform.useractivity_datetime")
            ->setFormat("j. n. Y H:i:s")
            ->setAlign("left")
            ->setSortable()
            ->setFilterDateRange();
        $grid->addColumnText("ip", "userform.useractivity_ip")
            ->setSortable()
            ->setFilterText();
        $grid->addColumnText("description", "userform.useractivity_description")
            ->setFilterText();
        $grid->setDefaultSort(["datetime" => "DESC"]);
        return $grid;
    }

    /**
     * Get Form object
     * Set translator if is set
     * @return Form
     */
    public function getForm(): Form
    {
        $form = $this->formFactory->create();
        // set translator
        if (!$form->getTranslator()) {
            $form->setTranslator($this->getTranslator());
        }
        $form->onSubmit[] = function () {
            $this->onRedraw();
        };
        return $form;
    }

    /**
     * getter user
     * @return User
     */
    public function getUser(): User
    {
        return $this->user;
    }

    /**
     * setter user
     *
     * @param User $user
     */
    public function setUser(User $user)
    {
        $this->user = $user;
    }

    /**
     * @return UserManagerService
     */
    public function getUserManagerService(): UserManagerService
    {
        return $this->userManagerService;
    }

    /**
     * @return GridFactory
     */
    public function getGridFactory(): GridFactory
    {
        return $this->gridFactory;
    }

    /**
     * @return FormFactory
     */
    public function getFormFactory(): FormFactory
    {
        return $this->formFactory;
    }

    /**
     * @return LanguageHelper
     */
    public function getLanguageHelper(): LanguageHelper
    {
        return $this->languageHelper;
    }

    /**
     * @return ITranslator
     */
    public function getTranslator(): ITranslator
    {
        if (!$this->translator) {
            $this->translator = FormTranslator::getInstance();
            $this->translator->addDictionary(
                (new FormTranslations($this->storage))->getDictionaries($this->languageHelper->detectLanguage())
            );
        }
        return $this->translator;
    }

    /**
     * @param ITranslator $translator
     * @return UserFormFactory
     */
    public function setTranslator(ITranslator $translator): UserFormFactory
    {
        $this->translator = $translator;
        return $this;
    }

    /**
     * @param Form $form
     * @param string $newPassword
     * @return bool
     */
    protected function checkPassword(Form $form, string $newPassword): bool
    {
        if (($passwordSafe = $this->authenticator
                ->isPasswordSafe($newPassword)) > 0
        ) {
            if ($passwordSafe == 2 or $passwordSafe == 6) {
                $form->addError("usermanager.user_new_password_min_length_1");
            }
            if ($passwordSafe == 4 or $passwordSafe == 6) {
                $form->addError("usermanager.user_new_password_too_simple");
            }
            return false;
        } else {
            return true;
        }
    }
}
