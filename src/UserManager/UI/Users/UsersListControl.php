<?php
declare(strict_types=1);

namespace Mepatek\UserManager\UI\Users;

use Mepatek\Components\UI\GridFactory;
use Mepatek\UserManager\Model\User;
use Mepatek\UserManager\Model\UserManagerService;
use Mepatek\UserManager\UI\UsersFormsFactory;
use Nette\Application\UI\Control;
use Ublaboo\DataGrid\Column\Action\Confirmation\StringConfirmation;
use Ublaboo\DataGrid\DataGrid;

class UsersListControl extends Control
{
    /** @var UsersFormsFactory */
    private $usersFormsFactory;
    /** @var UserManagerService */
    private $userManagerService;
    /** @var GridFactory */
    private $gridFactory;
    /** @var string */
    private $linkEdit;
    /** @var string */
    private $linkChangePassword;
    /** @var boolean */
    private $permittedDelete;

    /**
     * UsersListControl constructor.
     * @param UserManagerService $userManagerService
     * @param GridFactory $gridFactory
     * @param bool $permittedDelete
     * @param string|null $linkEdit
     * @param string|null $linkChangePassword
     */
    public function __construct(
        UsersFormsFactory $usersFormsFactory,
        UserManagerService $userManagerService,
        GridFactory $gridFactory,
        bool $permittedDelete = false,
        ?string $linkEdit = null,
        ?string $linkChangePassword = null
    ) {
        $this->usersFormsFactory = $usersFormsFactory;
        $this->userManagerService = $userManagerService;
        $this->gridFactory = $gridFactory;
        $this->permittedDelete = $permittedDelete;
        $this->linkEdit = $linkEdit;
        $this->linkChangePassword = $linkChangePassword;
    }

    public function render()
    {
        $template = $this->getTemplate();
        if (isset($this->parent->translator)) {
            $template->setTranslator($this->parent->translator);
        }

        //      if (isset($this->parent->translator)) {
        //          $template->setTranslator($this->parent->translator);
        //      }

        $template->render(__DIR__ . '/' . substr(__CLASS__, strrpos(__CLASS__, '\\') + 1) . '.latte');
    }

    /**
     * @param string $name
     * @return DataGrid
     * @throws \Ublaboo\DataGrid\Exception\DataGridColumnStatusException
     * @throws \Ublaboo\DataGrid\Exception\DataGridException
     */
    public function createComponentUsersListGrid($name)
    {
        $qb = $this->userManagerService->getUserFacade()->getUserQB(true, true, true);

        $grid = $this->gridFactory->create(
            $qb,
            "id",
            20,
            $this,
            $name
        );

        $grid->setColumnsHideable();

        $grid->addColumnText("userName", "usermanager.user_name")
            ->setSortable()
            ->setDefaultHide()
            ->setFilterText();
        $grid->addColumnText("fullName", "usermanager.user_full_name")
            ->setSortable()
            ->setFilterText();
        $grid->addColumnText("title", "usermanager.user_title")
            ->setDefaultHide()
            ->setFilterText();
        $grid->addColumnText("email", "usermanager.user_email")
            ->setSortable()
            ->setFilterText();
        $grid->addColumnText("phone", "usermanager.user_phone")
            ->setSortable()
            ->setDefaultHide()
            ->setFilterText();
        $grid->addColumnDateTime("created", "usermanager.user_created")
            ->setFormat("d.m.Y H:i:s")
            ->setAlign("left")
            ->setSortable()
            ->setDefaultHide()
            ->setFilterDateRange();
        $grid->addColumnDateTime("lastLogged", "usermanager.user_last_logged")
            ->setSortable()
            ->setFormat("d.m.Y H:i:s")
            ->setAlign("left")
            ->setFilterDateRange();
        $disabledColumn = $grid->addColumnStatus("disabled", "usermanager.user_disabled_caption")
            ->setSortable()
            ->setCaret(false)
            ->addOption(true, 'usermanager.user_disabled')
            ->setIcon("lock")
            ->setClass("btn-warning")
            ->endOption()
            ->addOption(false, 'usermanager.user_enabled')
            ->setIcon("unlock-alt")
            ->setClass("btn-success")
            ->endOption()
            ->setFilterSelect([
                "" => "usermanager.user_disabled_all",
                0 => "usermanager.user_enabled",
                1 => "usermanager.user_disabled",
            ])->setTranslateOptions();

        $grid->setRowCallback(function (User $item, $tr) {
            if ($item->isDeleted()) {
                $tr->addClass("kt-font-danger");
            }
        });
        $disabledColumn->onChange[] =
            function ($id, $new_value) {
                $user = $this->userManagerService->getUserFacade()->find((int)$id);
                if (!$user->isDeleted()) {
                    $user->setDisabled($new_value);
                    $this->userManagerService->getUserFacade()->saveUser($user);
                }
                if ($this->presenter->isAjax()) {
                    $this->presenter->redrawControl('flashes');
                    $this["usersListGrid"]->redrawItem($id);
                }
            };

        if ($this->linkEdit) {
            $grid->addAction("userEdit", "")
                ->setTitle("usermanager.user_action_edit")
                ->setIcon("pencil");
            $grid->allowRowsAction('userEdit', function(User $item): bool {
                return !$item->isDeleted();
            });
        }
        if ($this->linkChangePassword) {
            $grid->addAction("userChangePassword", "")
                ->setTitle("Change password")
                ->setIcon("user-secret");
            $grid->allowRowsAction('userChangePassword', function(User $item): bool {
                return !$item->isDeleted();
            });
        }
        if ($this->permittedDelete) {
            $grid->addAction("userDelete", "")
                ->setConfirmation(new StringConfirmation(
                    "Are you sure to delete user %s?", "fullName"
                ))
                ->setTitle("Delete user")
                ->setIcon("trash");
            $grid->allowRowsAction('userDelete', function(User $item): bool {
                return !$item->isDeleted();
            });
            $grid->addAction("userUnDelete", "")
                ->setConfirmation(new StringConfirmation(
                    "Are you sure to undelete user %s?", "fullName"
                ))
                ->setTitle("Undelete user")
                ->setIcon("recycle");
            $grid->allowRowsAction('userUnDelete', function(User $item): bool {
                return $item->isDeleted();
            });
        }
        return $grid;
    }

    /**
     * @param $id
     * @throws \Nette\Application\AbortException
     */
    public function handleUserEdit($id)
    {
        $this->getPresenter()->redirect($this->linkEdit, ["id" => $id]);
    }

    /**
     * @param $id
     * @throws \Nette\Application\AbortException
     */
    public function handleUserChangePassword($id)
    {
        $this->getPresenter()->redirect($this->linkChangePassword, ["id" => $id]);
    }

    /**
     * @param $id
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     * @throws \Doctrine\ORM\TransactionRequiredException
     */
    public function handleUserDelete($id)
    {
        if ($id) {
            $user = $this->userManagerService->getUserFacade()->find((int)$id);
            $this->usersFormsFactory->deleteUser($user);
        }
    }

    /**
     * @param $id
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     * @throws \Doctrine\ORM\TransactionRequiredException
     */
    public function handleUserUnDelete($id)
    {
        if ($id) {
            $user = $this->userManagerService->getUserFacade()->find((int)$id);
            $this->usersFormsFactory->undeleteUser($user);
        }
    }
}
